<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<body class="antialiased">
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    <div>
        Для подтверждения создания уведомления погоды на почту по городу {{$noticeDTO->getCity()}}
        в {{$noticeDTO->getSendAt()}}
        пройдите по ссылке
        <a href="{{$verificationUrl}}">
            {{$verificationUrl}}
        </a>
    </div>
    <div>
        Если вы не хотите получать уведомления или это письмо попало к вам по ошибке, то не предпринимайте никаких действий.
    </div>
</div>
</body>
</html>


