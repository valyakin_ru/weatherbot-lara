<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<body class="antialiased">
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
    @if ($weatherDTO->hasError())
        <p>Ошибка!</p>
        <p>Город <b>{{$weatherDTO->getCityName()}}</b> не найден!</p>
    @else
        <p>Погода в городе <b>{{$weatherDTO->getCityName()}} </b></p>
        <p>{{$weatherDTO->getWeather()->getDescription()}}</p>
        <p>Температура: {{$weatherDTO->getTemperature()}}</p>
        <p>Ощущается как: {{$weatherDTO->getTempFeelsLike()}}</p>
        <p>Минимальная: {{$weatherDTO->getTempMin()}} </p>
        <p>Максимальная: {{$weatherDTO->getTempMax()}}</p>
        <p>Ветер: {{$weatherDTO->getWindDirection()}}</p>
        <p>дует со скоростью: {{$weatherDTO->getWindSpeed()}}м/с</p>
    @endif
</div>
</body>
</html>


