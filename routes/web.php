<?php

use App\Infrastructure\Http\Controllers\Notification\EmailVerifyController;
use App\Infrastructure\Http\Controllers\Notification\EmailWeatherNotificationController;
use App\Infrastructure\Http\Controllers\Telegram\TelegramWebHookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "Бот погоды";
});

Route::post('/telegram/webhook', TelegramWebHookController::class);
Route::post('/notification/email', EmailWeatherNotificationController::class);
Route::get('/notification/verify', EmailVerifyController::class);
