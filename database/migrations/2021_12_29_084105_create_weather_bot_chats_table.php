<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherBotChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_bot_chats', function (Blueprint $table) {
            $table->id();
            $table->integer('messenger_type')->comment('тип мессенджера. 1 - Телеграмм')
                ->nullable(false)
                ->comment('тип мессенджера. 1 - Телеграмм');
            $table->string('chat_id')
                ->unique()
                ->nullable(false)
                ->comment('Номер чата в котором зарегистрирован бот');
            $table->boolean('active')
                ->default(false)
                ->comment('Признак активности бота. True - запущен, False - остановлен');
            $table->boolean('notification')
                ->default(true)
                ->comment('Признак наличия уведомлений(звука) от бота, для данного чата. True - есть, False - нет');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_bot_chats');
    }
}
