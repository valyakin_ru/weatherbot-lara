<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherEmailNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_email_notifications', function (Blueprint $table) {
            $table->id();
            $table->time('send_at')
                ->nullable(false)
                ->comment('Время на которое назначено оповещение');
            $table->string('email')
                ->nullable(false);
            $table->string('city')
                ->nullable(false)
                ->comment('Город по которому нужна погода');
            $table->boolean('active')
                ->comment('Признак активности email. True - можно отправлять, False - заблокирован');
            $table->string('verify_token')
                ->comment('Токен проверки на подлинность запроса');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_email_notifications');
    }
}
