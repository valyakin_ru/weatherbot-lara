<?php
/**
 * Description of AbstractBaseDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\DTO;

use ReflectionClass;

abstract class AbstractBaseDTO
{
    /**
     * Возвращает dto в виде массива
     *
     * @return array
     */
    public function toArray(): array
    {
        $array = [];
        foreach (array_keys($this->getProperties()) as $property) {
            $array[$property] = $this->{$property};
        }
        return $array;
    }

    /**
     * Возвращает набор свойств объекта в виде ключей массива с нулевыми значениями
     *
     * @return array
     */
    protected function getProperties(): array
    {
        $properties = [];
        foreach ((new ReflectionClass($this))->getProperties() as $property) {
            $properties[$property->getName()] = null;
        }
        // закомментировано удаление свойств со значениями по умолчанию
        /*foreach (array_keys(get_class_vars(__CLASS__)) as $property) {
            unset($properties[$property]);
        }*/
        return $properties;
    }
}
