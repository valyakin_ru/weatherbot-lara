<?php


namespace App\Application\Service\Telegram\DTO\Output\Transport;


use App\Application\Service\DTO\AbstractBaseDTO;


/**
 * Class TelegramSendButtonDTO
 * @package App\Application\Service\Telegram\DTO\Output\Transport
 */
class TelegramSendButtonDTO extends AbstractBaseDTO
{
    /**
     * Название блока кнопок
     *
     * @var string
     */
    protected string $text;

    /**
     * Номер чата
     *
     * @var string
     */
    protected string $chat_id;

    /**
     * Массив настроек кнопок
     * 'reply_markup' => [
     *     'resize_keyboard' => true - изменение размера кнопок
     *     'keyboard' => [
     *         [
     *             ['text' => 'Кнопка 1'], ['text' => 'Кнопка 2'],
     *         ],
     *         [
     *             ['text' => 'Кнопка 3'], ['text' => 'Кнопка 4'],
     *         ]
     *     ]
     * ]
     * @var array
     */
    protected array $reply_markup;

    /**
     * Звуковая нотификация появления сообщения
     * true - отключает звук
     * false - включает звук
     *
     * @var bool
     */
    protected bool $disable_notification;

    /**
     * @param string $text
     * @param string $chat_id
     * @param array $reply_markup
     * @param bool $disable_notification
     */
    public function __construct(
        string $text,
        string $chat_id,
        array $reply_markup,
        bool $disable_notification,
    )
    {
        $this->text = $text;
        $this->chat_id = $chat_id;
        $this->reply_markup = $reply_markup;
        $this->disable_notification = $disable_notification;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        return new static(
            text: $data['text'],
            chat_id: $data['chatId'],
            reply_markup: $data['replyMarkup'],
            disable_notification: $data['disableNotification'],
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'text' => $this->getText(),
            'chat_id' => $this->getChatId(),
            'reply_markup' => $this->getReplyMarkup(),
            'disable_notification' => $this->getDisableNotification(),
        ];
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getChatId(): string
    {
        return $this->chat_id;
    }

    /**
     * @return array
     */
    public function getReplyMarkup(): array
    {
        return $this->reply_markup;
    }

    /**
     * @return bool
     */
    public function getDisableNotification(): bool
    {
        return $this->disable_notification;
    }
}
