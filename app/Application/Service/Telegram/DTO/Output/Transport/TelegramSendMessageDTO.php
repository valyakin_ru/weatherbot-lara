<?php


namespace App\Application\Service\Telegram\DTO\Output\Transport;


use App\Application\Service\DTO\AbstractBaseDTO;

/**
 * Class TelegramSendMessageDTO
 * @package App\Application\Service\Telegram\DTO\Output\Transport
 */
class TelegramSendMessageDTO extends AbstractBaseDto
{
    /**
     * Текст сообщения
     *
     * @var string
     */
    protected string $text;

    /**
     * Номер чата
     *
     * @var string
     */
    protected string $chatId;

    /**
     * Email
     *
     * @var string|null
     */
    protected ?string $email;

    /**
     * Звуковая нотификация появления сообщения
     * true - отключает звук
     * false - включает звук
     *
     * @var bool
     */
    protected bool $disableNotification;


    /**
     * @param string $text
     * @param string $chatId
     * @param string|null $email
     * @param bool $disableNotification
     */
    public function __construct(
        string $text,
        string $chatId,
        string $email = null,
        bool $disableNotification = true
    )
    {
        $this->text = $text;
        $this->chatId = $chatId;
        $this->email = $email;
        $this->disableNotification = $disableNotification;
    }

    /**
     * @param array $data
     * @return TelegramSendMessageDTO
     */
    public static function fromArray(array $data): TelegramSendMessageDTO
    {
        return new TelegramSendMessageDTO(
            text:   $data['text'],
            chatId: $data['chatId'],
            email: $data['email'] ?? null,
            disableNotification: $data['disableNotification'],
        );
    }

    public function toArray(): array
    {
        return [
            'text' => $this->getText(),
            'chat_id' => $this->getChatId(),
            'disable_notification' => $this->getDisableNotification(),
        ];
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getChatId(): string
    {
        return $this->chatId;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }


    /**
     * @return bool
     */
    public function getDisableNotification(): bool
    {
        return $this->disableNotification;
    }

    /**
     * @param string|null $email
     * @return TelegramSendMessageDTO
     */
    public function setEmail(?string $email): TelegramSendMessageDTO
    {
        $this->email = $email;
        return $this;
    }
}
