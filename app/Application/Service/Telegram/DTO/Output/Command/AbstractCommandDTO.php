<?php
/**
 * Description of AbstractCommandDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Output\Command;

use App\Application\Service\DTO\AbstractBaseDTO;

abstract class AbstractCommandDTO extends AbstractBaseDTO
{
    protected int $chatId;

    /**
     * @param array $data
     * @return static
     */
    abstract public static function fromArray(array $data): self;
}
