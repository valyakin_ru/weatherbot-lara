<?php
/**
 * Description of TelegramInfoDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Output\Command;

class TelegramInfoDTO extends AbstractCommandDTO
{
    protected int $chatId;
    private array $botCommands;

    /**
     * @param int $chatId
     * @param array $botCommands
     */
    public function __construct(int $chatId, array $botCommands)
    {
        $this->chatId = $chatId;
        $this->botCommands = $botCommands;
    }

    /**
     * @param array $data
     * @return TelegramInfoDTO
     */
    public static function fromArray(array $data): TelegramInfoDTO
    {
        return new TelegramInfoDTO(
            chatId: $data['chatId'],
            botCommands: $data['botCommands'],
        );
    }

    /**
     * @return int
     */
    public function getChatId(): int
    {
        return $this->chatId;
    }

    /**
     * @return array
     */
    public function getBotCommands(): array
    {
        return $this->botCommands;
    }
}
