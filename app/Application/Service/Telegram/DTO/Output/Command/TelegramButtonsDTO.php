<?php
/**
 * Description of TelegramButtonsDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Output\Command;

class TelegramButtonsDTO extends AbstractCommandDTO
{
    private array $buttonsText;

    /**
     * Создание нового экземпляра TelegramButtonsDTO
     *
     * @param array $buttonsText
     */
    public function __construct(array $buttonsText)
    {
        $this->buttonsText = $buttonsText;
    }

    /**
     * Создает экземпляр класса из массива
     *
     * @param array $data
     * @return TelegramButtonsDTO
     */
    public static function fromArray(array $data): self
    {
        return new self(
            buttonsText: $data['buttonsText'],
        );
    }

    /**
     * @return array
     */
    public function getButtonsText(): array
    {
        $strings = [];
        foreach ($this->buttonsText as $buttonsString) {
            $result = [];
            foreach ($buttonsString as $button) {
                $result[] = ['text' => $button];
            };
            $strings[] = $result;
        }
        return $strings;
    }
}
