<?php
/**
 * Description of TelegramNoticeDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Output\Command;


use App\Application\Service\Notification\DTO\Output\VerificationDTOInterface;


class TelegramNoticeDTO
{

    private int $chatId;
    private VerificationDTOInterface $verificationDTO;


    /**
     * @param int $chatId
     * @param VerificationDTOInterface $verificationDTO
     */
    public function __construct(
        int $chatId,
        VerificationDTOInterface $verificationDTO
    )
    {
        $this->chatId = $chatId;
        $this->verificationDTO = $verificationDTO;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        return new self(
            chatId: $data['chatId'],
            verificationDTO: $data['verificationDTO'],
        );
    }

    public function toArray(): array
    {
        return [
            'chatId' => $this->getChatId(),
            'verificationDTO' => $this->getVerificationDTO()->toArray(),
        ];
    }

    /**
     * @return int
     */
    public function getChatId(): int
    {
        return $this->chatId;
    }

    /**
     * @return VerificationDTOInterface
     */
    public function getVerificationDTO(): VerificationDTOInterface
    {
        return $this->verificationDTO;
    }
}
