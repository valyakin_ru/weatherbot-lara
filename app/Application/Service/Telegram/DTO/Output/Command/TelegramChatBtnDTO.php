<?php
/**
 * Description of TelegramChatBtnDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Output\Command;

class TelegramChatBtnDTO
{
    private string $groupChatBtnText;
    private array $buttons;

    /**
     * @param string $groupChatBtnText
     * @param array $buttons
     */
    public function __construct(string $groupChatBtnText, array $buttons)
    {
        $this->groupChatBtnText = $groupChatBtnText;
        $this->buttons = $buttons;
    }

    public static function fromArray(array $data): TelegramChatBtnDTO
    {
        return new TelegramChatBtnDTO(
            groupChatBtnText: $data['groupChatBtnText'],
            buttons: $data['buttons'],
        );
    }

    /**
     * @return string
     */
    public function getGroupChatBtnText(): string
    {
        return $this->groupChatBtnText;
    }

    /**
     * @return array
     */
    public function getButtons(): array
    {
        return $this->buttons;
    }
}
