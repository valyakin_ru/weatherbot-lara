<?php
/**
 * Description of TelegramWeatherDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Output\Command;

use App\Application\Service\Weather\DTO\Output\WeatherOutputDTO;

class TelegramWeatherDTO extends AbstractCommandDTO
{
    protected int $chatId;
    private WeatherOutputDTO $weatherOutputDTO;

    /**
     * @param int $chatId
     * @param WeatherOutputDTO $weatherOutputDTO
     */
    public function __construct(
        int $chatId,
        WeatherOutputDTO $weatherOutputDTO,
    )
    {
        $this->chatId = $chatId;
        $this->weatherOutputDTO = $weatherOutputDTO;
    }

    /**
     * @param array $data
     * @return TelegramWeatherDTO
     */
    public static function fromArray(array $data): TelegramWeatherDTO
    {
        return new TelegramWeatherDTO(
            chatId: $data['chatId'],
            weatherOutputDTO: $data['weatherOutput'],
        );
    }

    public function toArray(): array
    {
        return [
            'chatId' => $this->getChatId(),
            'weatherOutput' => $this->weatherOutputDTO,
        ];
    }

    /**
     * @return int
     */
    public function getChatId(): int
    {
        return $this->chatId;
    }

    /**
     * @return WeatherOutputDTO
     */
    public function getWeatherOutputDTO(): WeatherOutputDTO
    {
        return $this->weatherOutputDTO;
    }
}
