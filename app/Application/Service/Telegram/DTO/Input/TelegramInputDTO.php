<?php
/**
 * Description of TelegramInputDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\DTO\Input;

use App\Application\Service\DTO\AbstractBaseDTO;

class TelegramInputDTO extends AbstractBaseDTO
{
    public int $update_id;
    public ?array $message;
    public ?array $callback_query;

    /**
     * @param int $update_id
     * @param array|null $message
     * @param array|null $callback_query
     */
    public function __construct(
        int $update_id,
        array $message = null,
        array $callback_query = null,
    )
    {
        $this->update_id = $update_id;
        $this->message = $message;
        $this->callback_query = $callback_query;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        return new static(
            update_id: $data['update_id'],
            message: $data['message'] ?? $data['edited_message'] ?? null,
            callback_query: $data['callback_query'] ?? null,
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'update_id' => $this->getUpdateId(),
            'message' => $this->getMessage(),
            'callback_query' => $this->getCallbackQuery(),
        ];
    }

    /**
     * @return int
     */
    public function getUpdateId(): int
    {
        return $this->update_id;
    }

    /**
     * @return array|null
     */
    public function getMessage(): ?array
    {
        return $this->message;
    }

    /**
     * @return array|null
     */
    public function getCallbackQuery(): ?array
    {
        return $this->callback_query;
    }

    /**
     * Возвращает сообщение из чата (callback или обычное)
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->getMessage()['text'];
    }

    /**
     * Возвращает Id чата
     * Номер чата при обычном вызове передается через массив 'message' с ключом ['chat']['id']
     * При callback_query он хранится в массиве 'callback_query' с ключом ['message']['chat']['id']
     *
     * @return int
     */
    public function getChatId(): int
    {
        return $this->getCallbackQuery()['message']['chat']['id'] ?? $this->getMessage()['chat']['id'];
    }
}
