<?php
/**
 * Description of TelegramWebHookParser.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram;


use App\Application\Exceptions\Telegram\TelegramCommandNotFoundException;
use App\Application\Exceptions\Telegram\TelegramBotNotStartedException;
use App\Application\Service\Telegram\Commands\TelegramCommandsContainers;
use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\Helper\ErrorCodes;
use App\Domain\Entities\Telegram\TelegramCommandInterface;
use App\Domain\Models\WeatherBotChat;


class TelegramWebHookParser
{

    private TelegramInputDTO $inputDTO;
    private WeatherBotChat $botChat;
    private TelegramCommandsContainers $commandsContainers;


    /**
     * @param TelegramInputDTO $inputDTO
     * @param WeatherBotChat $botChat
     * @param TelegramCommandsContainers $commandsContainers
     */
    public function __construct(
        TelegramInputDTO $inputDTO,
        WeatherBotChat $botChat,
        TelegramCommandsContainers $commandsContainers
    )
    {
        $this->inputDTO = $inputDTO;
        $this->botChat = $botChat;
        $this->commandsContainers = $commandsContainers;
    }

    /**
     * Возвращает команду, которую требуется выполнить
     *
     * @return TelegramCommandInterface
     * @throws TelegramCommandNotFoundException
     * @throws TelegramBotNotStartedException
     */
    public function getBotCommand(): TelegramCommandInterface
    {
        $commands = $this->commandsContainers->getAllCommands();
        $text = $this->getIncomeCommandText();
        $incomeCommand = explode(" ", $text);
        $incomeCommand[0] = explode("@", $incomeCommand[0])[0];
        if ($incomeCommand[0] !== '/start' && !$this->isBotStarted()) {
            throw new TelegramBotNotStartedException(ErrorCodes::getErrorMessage(ErrorCodes::BOT_IS_NOT_STARTED),ErrorCodes::BOT_IS_NOT_STARTED);
        }
        foreach ($commands as $command) {
            if ($command::COMMAND == $incomeCommand[0]) {
                return $command->setParameters($incomeCommand);
            }
        }
        throw new TelegramCommandNotFoundException(ErrorCodes::getErrorMessage(ErrorCodes::COMMAND_NOT_DEFINED),ErrorCodes::COMMAND_NOT_DEFINED);
    }

    /**
     * @return bool
     */
    private function isBotStarted(): bool
    {
        return $this->botChat->isActive();
    }

    /**
     * Возвращает текст команды.
     * Она может прийти как в массиве 'message', если это команда набранная в чате,
     * так и в массиве callback_query 'data', если команда была запущена кнопкой из чата,
     * сформированной /chatbtn
     *
     * @return string
     */
    private function getIncomeCommandText(): string
    {
        return mb_strtolower($this->inputDTO->getCallbackQuery()['data'] ?? $this->inputDTO->getText(), 'utf-8');
    }
}
