<?php
/**
 * Description of TelegramService.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram;

use App\Application\Exceptions\Telegram\TelegramCommandNotFoundException;
use App\Application\Exceptions\Telegram\TelegramBotNotStartedException;
use App\Application\Service\Telegram\Error\TelegramError;
use Exception;
use Illuminate\Support\Facades\Log;


class TelegramService
{
    const TELEGRAM_MESSENGER_TYPE = 1;
    const FATAL_ERROR_MESSAGE = 'Бот погоды сломан. Требуется починка.';
    const SERVICE_WORK_MESSAGE = 'Бот погоды находится на сервисном обслуживании. Повторите попытку позднее';


    private TelegramWebHookParser $webHookParser;
    private TelegramError $telegramError;


    /**
     * @param TelegramWebHookParser $webHookParser
     * @param TelegramError $telegramError
     */
    public function __construct(TelegramWebHookParser $webHookParser, TelegramError $telegramError)
    {
        $this->webHookParser = $webHookParser;
        $this->telegramError = $telegramError;
    }

    /**
     * Запуск работы бота
     */
    public function runBot(): void
    {
        if ($this->checkServiceWork()) {
            $this->sendErrorMessage(self::SERVICE_WORK_MESSAGE);
            return;
        }

        try {
            $this->webHookParser->getBotCommand()->execute();
        } catch (TelegramCommandNotFoundException $e) {
            Log::notice($e->getMessage());
        } catch (TelegramBotNotStartedException $e) {
            //
        } catch (Exception $e) {
            Log::alert($e->getMessage());
            $this->sendErrorMessage(self::FATAL_ERROR_MESSAGE);
        }
    }

    /**
     * @param string $message
     */
    public function sendErrorMessage(string $message): void
    {
        $this->telegramError->sendMessage($message);
    }

    private function checkServiceWork(): bool
    {
        return env('TELEGRAM_SERVICE_WORK');
    }
}
