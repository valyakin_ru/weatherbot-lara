<?php
/**
 * Description of BaseTelegramCommand.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands;

use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Domain\Models\WeatherBotChat;
use App\Infrastructure\Http\Response\Telegram\TelegramTransport;

class BaseTelegramCommand
{
    protected TelegramTransport $transport;
    protected WeatherBotChat $weatherBotChat;
    protected TelegramInputDTO $inputDTO;
    protected array $commandParameters;
    protected AbstractBaseTelegramCommandViewModel $viewModel;

    /**
     * @param TelegramTransport $transport
     * @param WeatherBotChat $weatherBotChat
     * @param TelegramInputDTO $inputDTO
     * @param AbstractBaseTelegramCommandViewModel $viewModel
     */
    public function __construct(
        TelegramTransport $transport,
        WeatherBotChat $weatherBotChat,
        TelegramInputDTO $inputDTO,
        AbstractBaseTelegramCommandViewModel $viewModel,
    )
    {
        $this->transport = $transport;
        $this->weatherBotChat = $weatherBotChat;
        $this->inputDTO = $inputDTO;
        $this->viewModel = $viewModel;
    }

    /**
     * @param array $parameters
     * @return $this
     */
    public function setParameters(array $parameters): BaseTelegramCommand
    {
        $this->commandParameters = $parameters;
        return $this;
    }
}
