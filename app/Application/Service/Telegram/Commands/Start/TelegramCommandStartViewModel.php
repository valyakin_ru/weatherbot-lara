<?php
/**
 * Description of TelegramCommandStartViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Start;


use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;


class TelegramCommandStartViewModel extends AbstractBaseTelegramCommandViewModel
{
    const ALREADY_STARTED = 'Бот уже был запущен';
    const JUST_STARTED = 'Бот успешно запущен';


    /**
     * Возвращает сообщение о запуске бота
     *
     * @return TelegramSendMessageDTO
     */
    public function getStartMessage(): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO(self::JUST_STARTED);
    }

    /**
     * Возвращает предупреждение, что бот уже запущен
     *
     * @return TelegramSendMessageDTO
     */
    public function getAlreadyStartedMessage(): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO(self::ALREADY_STARTED);
    }
}
