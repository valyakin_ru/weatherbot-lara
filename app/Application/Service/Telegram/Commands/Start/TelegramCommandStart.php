<?php
/**
 * Description of TelegramCommandStart.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Start;


use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Domain\Entities\Telegram\TelegramCommandInterface;


class TelegramCommandStart extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/start';
    const COMMAND_NAME = '/start';
    const COMMAND_DESCRIPTION = 'Запуск бота';


    public function execute(): void
    {
        if ($this->isBotStarted()) {
            $message = $this->viewModel->getAlreadyStartedMessage();
            $this->transport->sendMessage($message);
        } else {
            $this->start();
        }
    }

    /**
     * Запускает бот
     */
    private function start(): void
    {
        $this->weatherBotChat->saveActiveStatus(true);
        $message = $this->viewModel->getStartMessage();
        $this->transport->sendMessage($message);
    }

    /**
     * @return bool
     */
    private function isBotStarted(): bool
    {
        return $this->weatherBotChat->isActive();
    }
}
