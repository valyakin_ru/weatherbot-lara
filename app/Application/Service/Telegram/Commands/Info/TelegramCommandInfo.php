<?php
/**
 * Description of TelegramCommandInfo.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Info;


use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramInfoDTO;
use App\Domain\Entities\Telegram\TelegramCommandInterface;
use ReflectionClass;
use ReflectionException;


class TelegramCommandInfo extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/info';
    const COMMAND_NAME = '/info';
    const COMMAND_DESCRIPTION = 'информация о боте';

    private TelegramInfoDTO $infoDTO;


    /**
     * Запускает выполнение команды пришедшей, из телеграмма
     *
     * @throws ReflectionException
     */
    public function execute(): void
    {
        $this->setInfoDTO();
        $message = $this->viewModel->getInfoMessage($this->infoDTO);
        $this->transport->sendMessage($message);
    }

    /**
     * @throws ReflectionException
     */
    private function setInfoDTO()
    {
        $this->infoDTO = TelegramInfoDTO::fromArray([
            'chatId' => $this->inputDTO->getChatId(),
            'botCommands' => $this->getBotCommandsInfo()
        ]);
    }

    /**
     * Возвращает массив из команд и их описания
     *
     * @return array
     * @throws ReflectionException
     */
    private function getBotCommandsInfo(): array
    {
        $commands = [];
        foreach ($this->getCommandsClasses() as $command) {
            $commands[] = [
                'command' => $command::COMMAND_NAME ?? 'noCommand',
                'describe' => $command::COMMAND_DESCRIPTION ?? 'noDescription',
            ];
        }
        return $commands;
    }

    /**
     * Возвращает список классов которые являются командами телеграмма
     *
     * @throws ReflectionException
     */
    private function getCommandsClasses(): array
    {
        $classes = get_declared_classes();
        $implementsIModule = [];
        foreach($classes as $klass) {
            $reflect = new ReflectionClass($klass);
            if($reflect->implementsInterface(TelegramCommandInterface::class))
                $implementsIModule[] = $klass;
        }
        return $implementsIModule;
    }
}
