<?php
/**
 * Description of TelegramCommandInfoViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Info;


use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramInfoDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;


class TelegramCommandInfoViewModel extends AbstractBaseTelegramCommandViewModel
{

    /**
     * Возвращает набор команд бота в виде строки
     *
     * @param TelegramInfoDTO $infoDTO
     * @return string
     */
    private function botCommandsToString(TelegramInfoDTO $infoDTO): string
    {
        return array_reduce(
            $infoDTO->getBotCommands(),
            fn($carry, $item) => $carry .= $item['command'] . ' - ' . $item['describe'] . PHP_EOL,
            ''
        );
    }

    /**
     * Возвращает инфо сообщение
     *
     * @param TelegramInfoDTO $infoDTO
     * @return TelegramSendMessageDTO
     */
    public function getInfoMessage(TelegramInfoDTO $infoDTO): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO($this->botCommandsToString($infoDTO));
    }
}
