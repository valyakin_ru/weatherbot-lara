<?php
/**
 * Description of TelegramCommandsContainers.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands;


use App\Application\Service\Telegram\Commands\Buttons\TelegramCommandButtons;
use App\Application\Service\Telegram\Commands\Buttons\TelegramCommandButtonsViewModel;
use App\Application\Service\Telegram\Commands\ChatBtn\TelegramCommandChatBtn;
use App\Application\Service\Telegram\Commands\ChatBtn\TelegramCommandChatBtnViewModel;
use App\Application\Service\Telegram\Commands\Info\TelegramCommandInfo;
use App\Application\Service\Telegram\Commands\Info\TelegramCommandInfoViewModel;
use App\Application\Service\Telegram\Commands\Notice\TelegramCommandNoticeViewModel;
use App\Application\Service\Telegram\Commands\Notice\TelegramCommandNotice;
use App\Application\Service\Telegram\Commands\Sound\TelegramCommandSound;
use App\Application\Service\Telegram\Commands\Sound\TelegramCommandSoundViewModel;
use App\Application\Service\Telegram\Commands\Start\TelegramCommandStart;
use App\Application\Service\Telegram\Commands\Start\TelegramCommandStartViewModel;
use App\Application\Service\Telegram\Commands\Stop\TelegramCommandStop;
use App\Application\Service\Telegram\Commands\Stop\TelegramCommandStopViewModel;
use App\Application\Service\Telegram\Commands\Weather\TelegramCommandWeather;
use App\Application\Service\Telegram\Commands\Weather\TelegramCommandWeatherViewModel;
use Illuminate\Support\Facades\App;


class TelegramCommandsContainers
{
    public function getAllCommands(): array
    {
        return [
            $this->getCommandStartInstance(),
            $this->getCommandStopInstance(),
            $this->getCommandWeatherInstance(),
            $this->getCommandSoundInstance(),
            $this->getCommandButtonsInstance(),
            $this->getCommandChatBtnInstance(),
            $this->getCommandNoticeInstance(),
            $this->getCommandInfoInstance(),
        ];
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandInfo
     *
     * @return TelegramCommandInfo
     */
    private function getCommandInfoInstance(): TelegramCommandInfo
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandInfoViewModel::class);
        return app(TelegramCommandInfo::class);
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandStart
     *
     * @return TelegramCommandStart
     */
    private function getCommandStartInstance(): TelegramCommandStart
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandStartViewModel::class);
        return app(TelegramCommandStart::class);
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandStop
     *
     * @return TelegramCommandStop
     */
    private function getCommandStopInstance(): TelegramCommandStop
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandStopViewModel::class);
        return app(TelegramCommandStop::class);
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandWeather
     *
     * @return TelegramCommandWeather
     */
    private function getCommandWeatherInstance(): TelegramCommandWeather
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandWeatherViewModel::class);
        return app(TelegramCommandWeather::class);
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandSound
     *
     * @return TelegramCommandSound
     */
    private function getCommandSoundInstance(): TelegramCommandSound
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandSoundViewModel::class);
        return app(TelegramCommandSound::class);
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandButtons
     *
     * @return TelegramCommandButtons
     */
    private function getCommandButtonsInstance(): TelegramCommandButtons
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandButtonsViewModel::class);
        return app(TelegramCommandButtons::class);
    }

    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandChatBtnViewModel
     *
     * @return TelegramCommandChatBtn
     */
    private function getCommandChatBtnInstance(): TelegramCommandChatBtn
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandChatBtnViewModel::class);
        return app(TelegramCommandChatBtn::class);
    }
    /**
     * Возвращает подготовленный экземпляр класса TelegramCommandChatBtnViewModel
     *
     * @return TelegramCommandNotice
     */
    private function getCommandNoticeInstance(): TelegramCommandNotice
    {
        App::bind(AbstractBaseTelegramCommandViewModel::class, TelegramCommandNoticeViewModel::class);
        return app(TelegramCommandNotice::class);
    }
}
