<?php
/**
 * Description of TelegramCommandButtonView.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Buttons;

use App\Application\Service\Telegram\DTO\Output\Command\TelegramButtonsDTO;

class TelegramCommandButtonView
{
    /**
     * Возвращает массив кнопок
     *
     * @param TelegramButtonsDTO $buttonsDTO
     * @return array
     */
    public function renderButtons(TelegramButtonsDTO $buttonsDTO): array
    {
        return [
            'resize_keyboard' => true,
            'keyboard' => $buttonsDTO->getButtonsText()
        ];
    }

    /**
     * Возвращает признак для удаления кнопок
     *
     * @return bool[]
     */
    public function renderEmptyButtons(): array
    {
        return [
            'remove_keyboard' => true,
        ];
    }
}
