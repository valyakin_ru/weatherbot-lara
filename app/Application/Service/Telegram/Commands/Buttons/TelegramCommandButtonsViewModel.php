<?php
/**
 * Description of TelegramCommandButtonsViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Buttons;

use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramButtonsDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendButtonDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;
use App\Domain\Models\WeatherBotChat;


class TelegramCommandButtonsViewModel extends AbstractBaseTelegramCommandViewModel
{
    const BUTTONS_ON_TEXT = 'Кнопки загружены в панель';
    const BUTTONS_OFF_TEXT = 'Кнопки удалены из панели';
    const BUTTONS_ERROR_PARAMETER_TEXT = 'Не верный параметр команды';

    private TelegramCommandButtonView $view;

    /**
     * @param TelegramInputDTO $telegramInputDTO
     * @param WeatherBotChat $botChat
     * @param TelegramCommandButtonView $view
     */
    public function __construct(
        TelegramInputDTO $telegramInputDTO,
        WeatherBotChat $botChat,
        TelegramCommandButtonView $view,
    )
    {
        parent::__construct($telegramInputDTO, $botChat);
        $this->view = $view;
    }


    public function getButtonsOnMessage(TelegramButtonsDTO $buttonDTO): TelegramSendButtonDTO
    {
        return $this->getTelegramSendButtonDTO($this->getButtonsOnText(), $this->getButtons($buttonDTO));
    }

    public function getButtonsOffMessage(TelegramButtonsDTO $buttonDTO): TelegramSendButtonDTO
    {
        return $this->getTelegramSendButtonDTO($this->getButtonsOffText(), $this->getEmptyButtons());
    }

    public function getButtonsErrorMessage(TelegramButtonsDTO $buttonDTO): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO($this->getButtonsErrorParameterText());
    }

    private function getButtons(TelegramButtonsDTO $buttonDTO): array
    {
        return $this->view->renderButtons($buttonDTO);
    }

    private function getEmptyButtons(): array
    {
        return $this->view->renderEmptyButtons();
    }

    private function getButtonsOnText(): string
    {
        return self::BUTTONS_ON_TEXT;
    }

    private function getButtonsOffText(): string
    {
        return self::BUTTONS_OFF_TEXT;
    }

    private function getButtonsErrorParameterText(): string
    {
        return self::BUTTONS_ERROR_PARAMETER_TEXT;
    }
}
