<?php
/**
 * Description of TelegramCommandButtons.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Buttons;

use App\Application\Service\Repository\FilesRepository;
use App\Application\Service\Repository\RepositoryInterface;
use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramButtonsDTO;
use App\Domain\Entities\Telegram\TelegramCommandInterface;

class TelegramCommandButtons extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/buttons';
    const COMMAND_NAME = '/buttons on|off';
    const COMMAND_DESCRIPTION = 'Отображает кнопки';
    const CHAT_BUTTONS_FILES_PATH = __DIR__ . '/Files';

    private TelegramButtonsDTO $buttonsDTO;


    public function execute(): void
    {
        $this->setButtonsDTO();

        if ($this->isParameterOn()) {
            $message = $this->viewModel->getButtonsOnMessage($this->buttonsDTO);
        } else if ($this->isParameterOff()) {
            $message = $this->viewModel->getButtonsOffMessage($this->buttonsDTO);
        } else {
            $message = $this->viewModel->getButtonsErrorMessage($this->buttonsDTO);
            $this->transport->sendMessage($message);
            return;
        }

        $this->transport->sendButtons($message);
    }

    /**
     * Устанавливает TelegramButtonsDTO
     */
    private function setButtonsDTO(): void
    {
        $this->buttonsDTO = TelegramButtonsDTO::fromArray([
            'buttonsText' => $this->getButtons(),
        ]);
    }

    /**
     * Возвращает true если первый параметр у команды 'on'
     *
     * @return bool
     */
    private function isParameterOn(): bool
    {
        return empty($this->commandParameters[1]) || (isset($this->commandParameters[1]) && $this->commandParameters[1] === mb_strtolower('on', 'utf-8'));
    }

    /**
     * Возвращает true если первый параметр у команды 'off'
     *
     * @return bool
     */
    private function isParameterOff(): bool
    {
        return isset($this->commandParameters[1]) && $this->commandParameters[1] === mb_strtolower('off', 'utf-8');
    }

    /**
     * Возвращает набор кнопок
     *
     * @return array
     */
    private function getButtons(): array
    {
        $buttons = [];
        foreach ($this->getRepository()->getButtons(self::CHAT_BUTTONS_FILES_PATH) as $buttonsSet) {
            $buttons += $buttonsSet['buttons'];
        }
        return $buttons;
    }

    /**
     * Возвращает репозиторий в котором находятся данные по кнопкам
     *
     * @return RepositoryInterface
     */
    private function getRepository(): RepositoryInterface
    {
        return app(FilesRepository::class);
    }
}
