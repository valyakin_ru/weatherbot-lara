<?php
/**
 * Description of TelegramCommandStopViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Stop;


use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;


class TelegramCommandStopViewModel extends AbstractBaseTelegramCommandViewModel
{
    const JUST_STOPPED = 'Бот успешно остановлен';


    /**
     * Возвращает сообщение об остановке бота
     *
     * @return TelegramSendMessageDTO
     */
    public function getStopMessage(): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO(self::JUST_STOPPED);
    }
}
