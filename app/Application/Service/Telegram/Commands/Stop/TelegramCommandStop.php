<?php
/**
 * Description of TelegramCommandStop.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Stop;


use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Domain\Entities\Telegram\TelegramCommandInterface;


class TelegramCommandStop extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/stop';
    const COMMAND_NAME = '/stop';
    const COMMAND_DESCRIPTION = 'Остановка бота';


    public function execute(): void
    {
        $this->weatherBotChat->saveActiveStatus(false);
        $message = $this->viewModel->getStopMessage();
        $this->transport->sendMessage($message);
    }
}
