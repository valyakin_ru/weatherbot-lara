<?php
/**
 * Description of AbstractBaseTelegramCommandViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands;

use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendButtonDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;
use App\Domain\Models\WeatherBotChat;

abstract class AbstractBaseTelegramCommandViewModel
{

    protected TelegramInputDTO $inputDTO;
    protected WeatherBotChat $weatherBotChat;

    /**
     * @param TelegramInputDTO $telegramInputDTO
     * @param WeatherBotChat $botChat
     */
    public function __construct(
        TelegramInputDTO $telegramInputDTO,
        WeatherBotChat $botChat
    )
    {
        $this->weatherBotChat = $botChat;
        $this->inputDTO = $telegramInputDTO;
    }

    /**
     * Возвращает DTO для отправки в телеграмм
     *
     * @param string $text
     * @return TelegramSendMessageDTO
     */
    protected function getTelegramSendMessageDTO(string $text): TelegramSendMessageDTO
    {
        return TelegramSendMessageDTO::fromArray([
            'text' => $text,
            'chatId' => $this->inputDTO->getChatId(),
            'disableNotification' => $this->isNotificationDisabled(),
        ]);
    }

    /**
     * @param string $text
     * @param array $buttons
     * @return TelegramSendButtonDTO
     */
    protected function getTelegramSendButtonDTO(string $text, array $buttons): TelegramSendButtonDTO
    {
        return TelegramSendButtonDTO::fromArray([
            'text' => $text,
            'chatId' => $this->inputDTO->getChatId(),
            'replyMarkup' => $buttons,
            'disableNotification' => $this->isNotificationDisabled(),
        ]);
    }

    /**
     * @return bool
     */
    private function isNotificationDisabled():bool
    {
        return !$this->weatherBotChat->isNotification();
    }
}
