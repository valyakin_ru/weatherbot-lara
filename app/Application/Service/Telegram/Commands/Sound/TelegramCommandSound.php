<?php
/**
 * Description of TelegramCommandSound.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Sound;


use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Domain\Entities\Telegram\TelegramCommandInterface;


class TelegramCommandSound extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/sound';
    const COMMAND_NAME = '/sound';
    const COMMAND_DESCRIPTION = 'Включить/Выключить звук';


    public function execute(): void
    {
        if ($this->isSound()) {
            $this->saveSound(false);
            $message = $this->viewModel->getSoundOffMessage();
        } else {
            $this->saveSound(true);
            $message = $this->viewModel->getSoundOnMessage();
        }
        $this->transport->sendMessage($message);
    }

    /**
     * Сохраняет параметр настройки звука
     */
    private function saveSound(bool $attribute): void
    {
        $this->weatherBotChat->saveNotificationStatus($attribute);
    }

    /**
     * @return bool
     */
    private function isSound(): bool
    {
        return $this->weatherBotChat->isNotification();
    }
}
