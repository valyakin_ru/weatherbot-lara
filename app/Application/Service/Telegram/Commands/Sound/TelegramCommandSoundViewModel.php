<?php
/**
 * Description of TelegramCommandSoundViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Sound;


use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;


class TelegramCommandSoundViewModel extends AbstractBaseTelegramCommandViewModel
{
    const SOUND_ON = 'Звук включен';
    const SOUND_OFF = 'Звук выключен';


    /**
     * Возвращает сообщение о включении звука
     *
     * @return TelegramSendMessageDTO
     */
    public function getSoundOnMessage(): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO(self::SOUND_ON);
    }

    /**
     * Возвращает сообщение о выключении звука
     *
     * @return TelegramSendMessageDTO
     */
    public function getSoundOffMessage(): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO(self::SOUND_OFF);
    }
}
