<?php
/**
 * Description of TelegramCommandChatBtnViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\ChatBtn;

use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramChatBtnDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendButtonDTO;
use App\Domain\Models\WeatherBotChat;

class TelegramCommandChatBtnViewModel extends AbstractBaseTelegramCommandViewModel
{

    private TelegramCommandChatBtnView $view;

    /**
     * @param TelegramInputDTO $telegramInputDTO
     * @param WeatherBotChat $botChat
     * @param TelegramCommandChatBtnView $chatBtnView
     */
    public function __construct(
        TelegramInputDTO $telegramInputDTO,
        WeatherBotChat $botChat,
        TelegramCommandChatBtnView $chatBtnView
    )
    {
        parent::__construct($telegramInputDTO, $botChat);
        $this->view = $chatBtnView;
    }

    /**
     * @param TelegramChatBtnDTO $buttonDTO
     * @return TelegramSendButtonDTO
     */
    public function getButtons(TelegramChatBtnDTO $buttonDTO): TelegramSendButtonDTO
    {
        return $this->getTelegramSendButtonDTO(
            $buttonDTO->getGroupChatBtnText(),
            $this->view->renderButtons($buttonDTO)
        );
    }
}
