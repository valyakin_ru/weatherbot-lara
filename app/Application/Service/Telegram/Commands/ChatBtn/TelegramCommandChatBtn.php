<?php
/**
 * Description of TelegramCommandChatBtn.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\ChatBtn;


use App\Application\Service\Repository\FilesRepository;
use App\Application\Service\Repository\RepositoryInterface;
use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramChatBtnDTO;
use App\Domain\Entities\Telegram\TelegramCommandInterface;


class TelegramCommandChatBtn extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/chatbtn';
    const COMMAND_NAME = '/chatbtn';
    const COMMAND_DESCRIPTION = 'кнопки в окне чата';
    const CHAT_BUTTONS_FILES_PATH = __DIR__ . '/Files';

    private TelegramChatBtnDTO $buttonsDTO;


    public function execute(): void
    {
        foreach ($this->getGroupButtons(self::CHAT_BUTTONS_FILES_PATH) as $chatButtons) {
            $this->setButtonsDTO($chatButtons);
            $messageNearCity = $this->viewModel->getButtons($this->buttonsDTO);
            $this->transport->sendButtons($messageNearCity);
        }
    }

    /**
     * Устанавливает DTO с кнопками для чата
     *
     * @param array $buttons
     */
    private function setButtonsDTO(array $buttons): void
    {
        $this->buttonsDTO = TelegramChatBtnDTO::fromArray([
            'groupChatBtnText' => $this->getGroupChatBtnText($buttons),
            'buttons' => $this->getButtons($buttons),
        ]);
    }

    /**
     * Возвращает текст для группы кнопок
     *
     * @param array $buttons
     * @return string
     */
    private function getGroupChatBtnText(array $buttons): string
    {
        return $buttons['groupChatBtnText']  ?? '';
    }

    /**
     * Возвращает массив с кнопками
     *
     * @param array $data
     * @return array
     */
    private function getButtons(array $data): array
    {
        return $data['buttons']  ?? [];
    }

    /**
     * Возвращает данные для вывода кнопок
     *
     * @return array
     */
    private function getGroupButtons(): array
    {
        return $this->getRepository()->getChatButtons(self::CHAT_BUTTONS_FILES_PATH);
    }

    /**
     * Возвращает репозиторий в котором находятся данные по кнопкам
     *
     * @return RepositoryInterface
     */
    private function getRepository(): RepositoryInterface
    {
        return app(FilesRepository::class);
    }
}
