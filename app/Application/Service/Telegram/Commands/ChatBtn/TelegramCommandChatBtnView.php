<?php
/**
 * Description of TelegramCommandChatBtnView.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\ChatBtn;

use App\Application\Service\Telegram\DTO\Output\Command\TelegramChatBtnDTO;

class TelegramCommandChatBtnView
{
    /**
     * Возвращает массив кнопок
     *
     * @param TelegramChatBtnDTO $buttonsDTO
     * @return array
     */
    public function renderButtons(TelegramChatBtnDTO $buttonsDTO): array
    {
        return [
            'resize_keyboard' => true,
            'inline_keyboard' => [
                $buttonsDTO->getButtons()
            ]
        ];
    }
}
