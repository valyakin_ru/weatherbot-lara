<?php
/**
 * Description of TelegramCommandNotice.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Notice;


use App\Application\Exceptions\Email\EmailDomainNotFoundException;
use App\Application\Exceptions\Email\InvalidEmailFormatException;
use App\Application\Exceptions\Notification\InvalidMethodException;
use App\Application\Exceptions\Telegram\TelegramTimeParameterErrorException;
use App\Application\Service\Notification\DTO\Output\VerificationDTOInterface;
use App\Application\Service\Notification\EmailService;
use App\Application\Service\Notification\NotificationInterface;
use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramNoticeDTO;
use App\Domain\Entities\Telegram\TelegramCommandInterface;


class TelegramCommandNotice extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/notice';
    const COMMAND_NAME = '/notice email name@domain.ru city time|off';
    const COMMAND_DESCRIPTION = 'уведомления';
    const INVALID_NOTIFICATION_METHOD = 'Не верно определен метод оповещения';

    private TelegramNoticeDTO $noticeDTO;
    private NotificationInterface $service;


    public function execute(): void
    {
        try {
            $this->service = $this->getNotificationService();
            $this->setNoticeDTO();
        } catch (
            InvalidEmailFormatException |
            EmailDomainNotFoundException |
            TelegramTimeParameterErrorException |
            InvalidMethodException $e
        ) {
            $message = $this->viewModel->getErrorMessage($e->getMessage());
            $this->transport->sendMessage($message);
            return;
        }

        if ($this->noticeDTO->getVerificationDTO()->isActive()) {
            $this->service->notify($this->noticeDTO->getVerificationDTO());
        }

        $this->service->updateNotificationModel($this->noticeDTO->getVerificationDTO());

        $message = $this->viewModel->getMessage($this->noticeDTO);
        $this->transport->sendMessage($message);
    }

    /**
     * @return VerificationDTOInterface
     */
    private function getVerificationDTO(): VerificationDTOInterface
    {
        return $this->service->getVerificationDTO($this->commandParameters);
    }

    /**
     * Формирует noticeDTO на основе входных параметров
     */
    private function setNoticeDTO()
    {
        $this->noticeDTO = TelegramNoticeDTO::fromArray([
            'chatId' => $this->inputDTO->getChatId(),
            'verificationDTO' => $this->getVerificationDTO(),
        ]);
    }

    /**
     * @return string
     */
    private function getNotificationMethod(): string
    {
        return $this->commandParameters[1] ?? '';
    }

    /**
     * @return NotificationInterface
     * @throws InvalidMethodException
     */
    private function getNotificationService(): NotificationInterface
    {
        return match ($this->getNotificationMethod()) {
            'email' => app(EmailService::class),
            default => throw new InvalidMethodException(self::INVALID_NOTIFICATION_METHOD),
        };
    }
}
