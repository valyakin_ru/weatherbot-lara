<?php
/**
 * Description of TelegramCommandNoticeViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Notice;

use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramNoticeDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;

class TelegramCommandNoticeViewModel extends AbstractBaseTelegramCommandViewModel
{
    const NOTIFICATION_OFF = 'Уведомление на почту остановлено';
    const NOTIFICATION_ON = 'Подтвердите установку уведомления. Инструкция отправлена по почте';

    /**
     * @param TelegramNoticeDTO $noticeDTO
     * @return TelegramSendMessageDTO
     */
    public function getMessage(TelegramNoticeDTO $noticeDTO): TelegramSendMessageDTO
    {
        return $noticeDTO->getVerificationDTO()->isActive()
            ? $this->getTelegramSendMessageDTO(self::NOTIFICATION_ON)
            : $this->getTelegramSendMessageDTO(self::NOTIFICATION_OFF);
    }

    /**
     * @param string $message
     * @return TelegramSendMessageDTO
     */
    public function getErrorMessage(string $message): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO($message);
    }
}
