<?php
/**
 * Description of TelegramCommandWeatherView.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Weather;

use App\Application\Service\Telegram\DTO\Output\Command\TelegramWeatherDTO;
use App\Application\Service\Telegram\Helper\ErrorCodes;

class TelegramCommandWeatherView
{
    private TelegramWeatherDTO $dto;

    /**
     * @param TelegramWeatherDTO $dto
     */
    public function __construct(TelegramWeatherDTO $dto)
    {
        $this->dto = $dto;
    }

    /**
     * Формирует вывод
     *
     * @return string
     */
    public function render(): string
    {
        return 'Сейчас в городе ' . $this->dto->getWeatherOutputDTO()->getCityName() . ' -' . PHP_EOL
            . $this->dto->getWeatherOutputDTO()->getWeather()->getDescription() . PHP_EOL
            . 'Температура: ' . $this->dto->getWeatherOutputDTO()->getTemperature() . PHP_EOL
            . 'Ощущается как: ' . $this->dto->getWeatherOutputDTO()->getTempFeelsLike() . PHP_EOL
            . 'Минимальная: ' . $this->dto->getWeatherOutputDTO()->getTempMin() . PHP_EOL
            . 'Максимальная: ' . $this->dto->getWeatherOutputDTO()->getTempMax() . PHP_EOL
            . 'Ветер: ' . $this->dto->getWeatherOutputDTO()->getWindDirection() . PHP_EOL
            . 'дует со скоростью: ' . $this->dto->getWeatherOutputDTO()->getWindSpeed() . 'м/с' . PHP_EOL;
    }

    public function renderError(): string
    {
        return 'Ошибка: ' . ErrorCodes::getErrorMessage($this->dto->getWeatherOutputDTO()->getResponseCode());
    }
}
