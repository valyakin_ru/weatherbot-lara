<?php
/**
 * Description of TelegramCommandWeather.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Weather;

use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\Commands\BaseTelegramCommand;
use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramWeatherDTO;
use App\Application\Service\Weather\DTO\Output\WeatherOutputDTO;
use App\Application\Service\Weather\WeatherService;
use App\Domain\Entities\Telegram\TelegramCommandInterface;
use App\Domain\Models\WeatherBotChat;
use App\Infrastructure\Http\Response\Telegram\TelegramTransport;


class TelegramCommandWeather extends BaseTelegramCommand implements TelegramCommandInterface
{
    const COMMAND = '/weather';
    const COMMAND_NAME = '/weather city';
    const COMMAND_DESCRIPTION = 'Погода для города';

    private TelegramWeatherDTO $weatherDTO;
    private WeatherService $weatherService;
    private WeatherOutputDTO $weatherOutputDTO;


    /**
     * @param TelegramTransport $transport
     * @param WeatherService $weatherService
     * @param WeatherBotChat $weatherBotChat
     * @param TelegramInputDTO $inputDTO
     * @param AbstractBaseTelegramCommandViewModel $viewModel
     */
    public function __construct(
        TelegramTransport $transport,
        WeatherService $weatherService,
        WeatherBotChat $weatherBotChat,
        TelegramInputDTO $inputDTO,
        AbstractBaseTelegramCommandViewModel $viewModel,
    )
    {
        parent::__construct(
            transport: $transport,
            weatherBotChat: $weatherBotChat,
            inputDTO: $inputDTO,
            viewModel: $viewModel
        );
        $this->weatherService = $weatherService;
    }

    public function execute(): void
    {
        $this->weatherOutputDTO = $this->weatherService->getWeather($this->getCity());
        $this->setWeatherDTO();
        $message = $this->viewModel->getWeatherMessage($this->weatherDTO);
        $this->transport->sendMessage($message);
    }

    /**
     * формирует weatherDTO на основе inputDTO
     */
    private function setWeatherDTO()
    {
        $this->weatherDTO = TelegramWeatherDTO::fromArray(
            array_merge(
                ['chatId' => $this->inputDTO->getChatId()],
                ['weatherOutput' => $this->weatherOutputDTO]
            )
        );
    }

    /**
     * @return string
     */
    private function getCity(): string
    {
        return $this->commandParameters[1] ?? env('WEATHER_DEFAULT_CITY_NAME');
    }
}
