<?php
/**
 * Description of TelegramCommandWeatherViewModel.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Commands\Weather;

use App\Application\Service\Telegram\Commands\AbstractBaseTelegramCommandViewModel;
use App\Application\Service\Telegram\DTO\Output\Command\TelegramWeatherDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;

class TelegramCommandWeatherViewModel extends AbstractBaseTelegramCommandViewModel
{

    /**
     * @param TelegramWeatherDTO $weatherDTO
     * @return TelegramSendMessageDTO
     */
    public function getWeatherMessage(TelegramWeatherDTO $weatherDTO): TelegramSendMessageDTO
    {
        return $this->getTelegramSendMessageDTO($this->weatherInfoToString($weatherDTO));
    }

    /**
     * Возвращает информацию о погоде в виде форматированной строки.
     * Для формирования данных, используются свойства DTO указанные в массиве $weatherInfo
     *
     * @param TelegramWeatherDTO $weatherDTO
     * @return string
     */
    private function weatherInfoToString(TelegramWeatherDTO $weatherDTO): string
    {
        return $weatherDTO->getWeatherOutputDTO()->hasError()
            ? (new TelegramCommandWeatherView($weatherDTO))->renderError()
            : (new TelegramCommandWeatherView($weatherDTO))->render();
    }
}
