<?php
/**
 * Description of ErrorCodes.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Helper;

class ErrorCodes
{
    const DEFAULT_ERROR = 'Неизвестная ошибка';

    const CITY_NOT_FOUND = 404;
    const BOT_IS_NOT_STARTED = 420;
    const COMMAND_NOT_DEFINED = 421;
    const WEATHER_SERVICE_UNAVAILABLE = 500;

    /**
     * Массив значений ошибок по кодам
     * 'код' => 'Описание ошибки'
     *
     * @var array|string[]
     */
    public static array $errors = [
        self::CITY_NOT_FOUND => 'Город не найден',
        self::WEATHER_SERVICE_UNAVAILABLE => 'Сервис погоды временно недоступен',
        self::COMMAND_NOT_DEFINED => 'Вызванная ботом команда не определена',
        self::BOT_IS_NOT_STARTED => 'Бот не запущен',
    ];

    /**
     * Возвращает сообщение об ошибке по коду
     *
     * @param int $errorCode
     * @return string
     */
    public static function getErrorMessage(int $errorCode): string
    {
        return self::$errors[$errorCode] ?? self::DEFAULT_ERROR;
    }
}
