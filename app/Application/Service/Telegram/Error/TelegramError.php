<?php
/**
 * Description of TelegramError.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Telegram\Error;

use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;
use App\Domain\Models\WeatherBotChat;
use App\Infrastructure\Http\Response\Telegram\TelegramTransport;

class TelegramError
{

    private TelegramInputDTO $inputDTO;
    private TelegramTransport $transport;
    private WeatherBotChat $weatherBotChat;

    /**
     * @param TelegramInputDTO $inputDTO
     * @param TelegramTransport $transport
     * @param WeatherBotChat $weatherBotChat
     */
    public function __construct(TelegramInputDTO $inputDTO, TelegramTransport $transport, WeatherBotChat $weatherBotChat)
    {
        $this->inputDTO = $inputDTO;
        $this->transport = $transport;
        $this->weatherBotChat = $weatherBotChat;
    }

    /**
     * @param $text
     */
    public function sendMessage($text): void
    {
        $this->transport->sendMessage(TelegramSendMessageDTO::fromArray([
            'text' => $text,
            'chatId' => $this->inputDTO->getChatId(),
            'disableNotification' => !$this->weatherBotChat->isNotification(),
        ]));
    }
}
