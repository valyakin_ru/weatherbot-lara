<?php
/**
 * Description of EmailValidator.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Validation\Email;

use App\Application\Exceptions\Email\EmailDomainNotFoundException;
use App\Application\Exceptions\Email\InvalidEmailFormatException;

class EmailValidator
{

    const INVALID_EMAIL_FORMAT = 'Не верный формат электронного адреса';
    const EMAIL_DOMAIN_NOT_FOUND = 'Не корректный домен электронной почты';

    /**
     * Возвращает структурированных список Email-ов
     *
     * @param array $emails
     * @return array
     */
    public function getFormattedEmailList(array $emails): array
    {
        $list = [];
        foreach ($emails as $email) {
            $list += $this->getFormattedEmail($email);
        }
        return $list;
    }

    /**
     * Возвращает структурированный email
     *
     * ["name"=>имя, "domain"=>домен, "zone"=>зона];
     *
     * @param string $email
     * @return array
     */
    public function getFormattedEmail(string $email): array
    {
        $result = [];
        $parse = explode("@", $email);
        $name = $parse[0];
        $domain = preg_replace("/(.[^.]+)$/", "", $parse[1]);
        preg_match('/([^.]+)$/', $parse[1], $matches);
        $zone = $matches[0];
        $result[$email] = ["name"=>$name, "domain"=>$domain, "zone"=>$zone];
        return $result;
    }

    /**
     * Возвращает true если email валидный
     *
     * @param string $email
     * @return bool
     * @throws EmailDomainNotFoundException
     * @throws InvalidEmailFormatException
     */
    public function isValidEmail(string $email):bool
    {
        return ($this->checkValid($email)
            && $this->checkMX($this->getDomain($email)));
    }

    /**
     * Возвращает домен из почтового ящика
     *
     * @param string $email
     * @return string
     */
    private function getDomain(string $email):string
    {
        return substr(strrchr($email, "@"), 1);
    }

    /**
     * Проверяет на правильность синтаксиса почтового ящика
     * возвращает true если формат email соответствует стандарту
     *
     * @param string $email
     * @return bool
     * @throws InvalidEmailFormatException
     */
    private function checkValid(string $email): bool
    {
        if (!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $email)) {
            throw new InvalidEmailFormatException(self::INVALID_EMAIL_FORMAT);
        } else return true;
    }

    /**
     * Возвращает true если для домена существует MX запись
     *
     * @param $domain
     * @return bool
     * @throws EmailDomainNotFoundException
     */
    private function checkMX($domain):bool
    {
        $result = getmxrr($domain, $mx_records, $mx_weight);
        if ((false == $result
            || 0 == count($mx_records)
            || (1 == count($mx_records)
                && ($mx_records[0] == null
                    || $mx_records[0] == "0.0.0.0")))) {
            throw new EmailDomainNotFoundException(self::EMAIL_DOMAIN_NOT_FOUND);
        } else return true;
    }
}
