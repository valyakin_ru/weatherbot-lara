<?php
/**
 * Description of WeatherService.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather;

use App\Application\Contract\Weather\WeatherInterface;

class WeatherService
{
    private WeatherInterface $weather;

    /**
     * @param WeatherInterface $weather
     */
    public function __construct(WeatherInterface $weather)
    {
        $this->weather = $weather;
    }

    /**
     * @param string $city
     * @return DTO\Output\WeatherOutputDTO
     */
    public function getWeather(string $city)
    {
        return $this->weather->getWeather($city);
    }

}
