<?php
/**
 * Description of OpenWeatherCoordinationDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

use App\Application\Service\DTO\AbstractBaseDTO;

class OpenWeatherCoordinationDTO extends AbstractBaseDTO
{
    protected float $lon;
    protected float $lat;

    /**
     * @param float $lon
     * @param float $lat
     */
    public function __construct(float $lon, float $lat)
    {
        $this->lon = $lon;
        $this->lat = $lat;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): OpenWeatherCoordinationDTO
    {
        return new OpenWeatherCoordinationDTO(
            lon:$data['lon'] ?? 0,
            lat:$data['lat'] ?? 0,
        );
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }
}
