<?php
/**
 * Description of OpenWeatherCloudsDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

use App\Application\Service\DTO\AbstractBaseDTO;

class OpenWeatherCloudsDTO extends AbstractBaseDTO
{
    protected int $all;

    /**
     * @param int $all
     */
    public function __construct(int $all)
    {
        $this->all = $all;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): OpenWeatherCloudsDTO
    {
        return new OpenWeatherCloudsDTO(
            all:$data['all'] ?? 0,
        );
    }

    /**
     * @return int
     */
    public function getAll(): int
    {
        return $this->all;
    }
}
