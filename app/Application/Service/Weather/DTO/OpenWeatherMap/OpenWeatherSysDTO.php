<?php
/**
 * Description of OpenWeatherSysDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

use App\Application\Service\DTO\AbstractBaseDTO;

class OpenWeatherSysDTO extends AbstractBaseDTO
{
    protected int $type;
    protected int $id;
    protected string $country;
    protected int $sunrise;
    protected int $sunset;
    protected string $sunriseFormatted;
    protected string $sunsetFormatted;

    /**
     * @param int $type
     * @param int $id
     * @param string $country
     * @param int $sunrise
     * @param int $sunset
     */
    public function __construct(
        int $type,
        int $id,
        string $country,
        int $sunrise,
        int $sunset,
    )
    {
        $this->type = $type;
        $this->id = $id;
        $this->country = $country;
        $this->sunrise = $sunrise;
        $this->sunset = $sunset;
        $this->sunriseFormatted = date('m/d/Y H:i:s', $sunrise);
        $this->sunsetFormatted = date('m/d/Y H:i:s', $sunset);
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): OpenWeatherSysDTO
    {
        return new OpenWeatherSysDTO(
            type:$data['type'] ?? 0,
            id:$data['id'] ?? 0,
            country:$data['country'] ?? '',
            sunrise:$data['sunrise'] ?? 0,
            sunset:$data['sunset'] ?? 0,
        );
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return int
     */
    public function getSunrise(): int
    {
        return $this->sunrise;
    }

    /**
     * @return int
     */
    public function getSunset(): int
    {
        return $this->sunset;
    }

    /**
     * @return string
     */
    public function getSunriseFormatted(): string
    {
        return $this->sunriseFormatted;
    }

    /**
     * @return string
     */
    public function getSunsetFormatted(): string
    {
        return $this->sunsetFormatted;
    }
}
