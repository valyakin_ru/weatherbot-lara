<?php
/**
 * Description of OpenWeatherGeneralDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

use App\Application\Service\DTO\AbstractBaseDTO;

class OpenWeatherGeneralDTO extends AbstractBaseDTO
{
    protected float $temperature;
    protected float $feelsLike;
    protected float $tempMin;
    protected float $tempMax;
    protected float $pressure;
    protected float $humidity;
    protected float $seaLevel;
    protected float $groundLevel;

    /**
     * @param float $temperature
     * @param float $feelsLike
     * @param float $tempMin
     * @param float $tempMax
     * @param float $pressure
     * @param float $humidity
     * @param float $seaLevel
     * @param float $groundLevel
     */
    public function __construct(
        float $temperature,
        float $feelsLike,
        float $tempMin,
        float $tempMax,
        float $pressure,
        float $humidity,
        float $seaLevel,
        float $groundLevel,
    )
    {
        $this->temperature = $temperature;
        $this->feelsLike = $feelsLike;
        $this->tempMin = $tempMin;
        $this->tempMax = $tempMax;
        $this->pressure = $pressure;
        $this->humidity = $humidity;
        $this->seaLevel = $seaLevel;
        $this->groundLevel = $groundLevel;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): OpenWeatherGeneralDTO
    {
        return new OpenWeatherGeneralDTO(
            temperature:$data['temp'] ?? 0,
            feelsLike:$data['feels_like'] ?? 0,
            tempMin:$data['temp_min'] ?? 0,
            tempMax:$data['temp_max'] ?? 0,
            pressure:$data['pressure'] ?? 0,
            humidity:$data['humidity'] ?? 0,
            seaLevel:$data['sea_level'] ?? 0,
            groundLevel:$data['grnd_level'] ?? 0,
        );
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }

    /**
     * @return float
     */
    public function getFeelsLike(): float
    {
        return $this->feelsLike;
    }

    /**
     * @return float
     */
    public function getTempMin(): float
    {
        return $this->tempMin;
    }

    /**
     * @return float
     */
    public function getTempMax(): float
    {
        return $this->tempMax;
    }

    /**
     * @return float
     */
    public function getPressure(): float
    {
        return $this->pressure;
    }

    /**
     * @return float
     */
    public function getHumidity(): float
    {
        return $this->humidity;
    }

    /**
     * @return float
     */
    public function getSeaLevel(): float
    {
        return $this->seaLevel;
    }

    /**
     * @return float
     */
    public function getGroundLevel(): float
    {
        return $this->groundLevel;
    }
}
