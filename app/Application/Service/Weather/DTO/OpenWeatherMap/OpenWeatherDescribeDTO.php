<?php
/**
 * Description of OpenWeatherDescribeDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

use App\Application\Service\DTO\AbstractBaseDTO;

class OpenWeatherDescribeDTO extends AbstractBaseDTO
{
    protected int $id;
    protected string $main;
    protected string $description;
    protected string $icon;

    /**
     * @param int $id
     * @param string $main
     * @param string $description
     * @param string $icon
     */
    public function __construct(
        int $id,
        string $main,
        string $description,
        string $icon,
    )
    {
        $this->id = $id;
        $this->main = $main;
        $this->description = $description;
        $this->icon = $icon;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): OpenWeatherDescribeDTO
    {
        return new OpenWeatherDescribeDTO(
            id:         $data['id'] ?? 0,
            main:       $data['main'] ?? '',
            description:$data['description'] ?? '',
            icon:       $data['icon'] ?? '',
        );
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMain(): string
    {
        return $this->main;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }
}
