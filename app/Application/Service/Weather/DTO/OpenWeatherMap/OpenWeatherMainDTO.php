<?php
/**
 * Description of OpenWeatherMainDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

class OpenWeatherMainDTO
{
    protected OpenWeatherCoordinationDTO $coordination;
    protected OpenWeatherDescribeDTO $weather;
    protected string $base;
    protected OpenWeatherGeneralDTO $main;
    protected int $visibility;
    protected OpenWeatherWindDTO $wind;
    protected OpenWeatherCloudsDTO $clouds;
    protected int $dt;
    protected OpenWeatherSysDTO $sys;
    protected int $timezone;
    protected int $id;
    protected string $cityName;
    protected int $responseCode;

    /**
     * @param array $coordination
     * @param array $weather
     * @param string $base
     * @param array $main
     * @param int $visibility
     * @param array $wind
     * @param array $clouds
     * @param int $dt
     * @param array $sys
     * @param int $timezone
     * @param int $id
     * @param string $cityName
     * @param int $responseCode
     */
    public function __construct(
        array $coordination,
        array $weather,
        string $base,
        array $main,
        int $visibility,
        array $wind,
        array $clouds,
        int $dt,
        array $sys,
        int $timezone,
        int $id,
        string $cityName,
        int $responseCode,
    )
    {
        $this->coordination = OpenWeatherCoordinationDTO::fromArray($coordination);
        $this->weather = OpenWeatherDescribeDTO::fromArray($weather[0] ?? $weather);
        $this->base = $base;
        $this->main = OpenWeatherGeneralDTO::fromArray($main);
        $this->visibility = $visibility;
        $this->wind = OpenWeatherWindDTO::fromArray($wind);
        $this->clouds = OpenWeatherCloudsDTO::fromArray($clouds);
        $this->dt = $dt;
        $this->sys = OpenWeatherSysDTO::fromArray($sys);;
        $this->timezone = $timezone;
        $this->id = $id;
        $this->cityName = $cityName;
        $this->responseCode = $responseCode;
    }

    public static function fromArray(array $data): OpenWeatherMainDTO
    {
        return new OpenWeatherMainDTO(
            coordination: $data['coord'] ?? [],
            weather: $data['weather'] ?? [],
            base: $data['base'] ?? '',
            main: $data['main'] ?? [],
            visibility: $data['visibility'] ?? 0,
            wind: $data['wind'] ?? [],
            clouds: $data['clouds'] ?? [],
            dt: $data['dt'] ?? 0,
            sys: $data['sys'] ?? [],
            timezone: $data['timezone'] ?? 0,
            id: $data['id'] ?? 0,
            cityName: $data['name'] ?? '',
            responseCode: (int)$data['cod'] ?? 0,
        );
    }

    public function toArray(): array
    {
        return [
            'coordination' => $this->getCoordination()->toArray(),
            'weather' => $this->getWeather()->toArray(),
            'base' => $this->getBase(),
            'main' => $this->getMain()->toArray(),
            'visibility' => $this->getVisibility(),
            'wind' => $this->getWind()->toArray(),
            'clouds' => $this->getClouds()->toArray(),
            'dt' => $this->getDt(),
            'sys' => $this->getSys()->toArray(),
            'timezone' => $this->getTimezone(),
            'id' => $this->getId(),
            'cityName' => $this->getCityName(),
            'responseCode' => $this->getResponseCode(),
        ];
    }

    /**
     * @return OpenWeatherCoordinationDTO
     */
    public function getCoordination(): OpenWeatherCoordinationDTO
    {
        return $this->coordination;
    }

    /**
     * @return OpenWeatherDescribeDTO
     */
    public function getWeather(): OpenWeatherDescribeDTO
    {
        return $this->weather;
    }

    /**
     * @return string
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * @return OpenWeatherGeneralDTO
     */
    public function getMain(): OpenWeatherGeneralDTO
    {
        return $this->main;
    }

    /**
     * @return int
     */
    public function getVisibility(): int
    {
        return $this->visibility;
    }

    /**
     * @return OpenWeatherWindDTO
     */
    public function getWind(): OpenWeatherWindDTO
    {
        return $this->wind;
    }

    /**
     * @return OpenWeatherCloudsDTO
     */
    public function getClouds(): OpenWeatherCloudsDTO
    {
        return $this->clouds;
    }

    /**
     * @return int
     */
    public function getDt(): int
    {
        return $this->dt;
    }

    /**
     * @return OpenWeatherSysDTO
     */
    public function getSys(): OpenWeatherSysDTO
    {
        return $this->sys;
    }

    /**
     * @return int
     */
    public function getTimezone(): int
    {
        return $this->timezone;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->responseCode !== 200;
    }
}
