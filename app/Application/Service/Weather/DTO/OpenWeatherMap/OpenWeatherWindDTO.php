<?php
/**
 * Description of OpenWeatherWindDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\OpenWeatherMap;

use App\Application\Service\DTO\AbstractBaseDTO;

class OpenWeatherWindDTO extends AbstractBaseDTO
{
    protected float $speed;
    protected int $deg;

    /**
     * @param float $speed
     * @param int $deg
     */
    public function __construct(float $speed, int $deg)
    {
        $this->speed = $speed;
        $this->deg = $deg;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): OpenWeatherWindDTO
    {
        return new OpenWeatherWindDTO(
            speed:$data['speed'] ?? 0,
            deg:$data['deg'] ?? 0,
        );
    }

    /**
     * @return float
     */
    public function getSpeed(): float
    {
        return $this->speed;
    }

    /**
     * @return int
     */
    public function getDeg(): int
    {
        return $this->deg;
    }
}
