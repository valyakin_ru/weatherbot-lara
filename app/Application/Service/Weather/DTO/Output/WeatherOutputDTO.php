<?php
/**
 * Description of WeatherOutputDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\Output;

use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherCloudsDTO;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherCoordinationDTO;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherDescribeDTO;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherGeneralDTO;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherMainDTO;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherSysDTO;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherWindDTO;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class WeatherOutputDTO implements Responsable
{

    protected OpenWeatherCoordinationDTO $coordination;
    protected OpenWeatherDescribeDTO $weather;
    protected string $base;
    protected OpenWeatherGeneralDTO $main;
    protected int $visibility;
    protected OpenWeatherWindDTO $wind;
    protected OpenWeatherCloudsDTO $clouds;
    protected int $dt;
    protected OpenWeatherSysDTO $sys;
    protected int $timezone;
    protected string $cityName;
    protected int $responseCode;

    /**
     * @param OpenWeatherCoordinationDTO $coordination
     * @param OpenWeatherDescribeDTO $weather
     * @param string $base
     * @param OpenWeatherGeneralDTO $main
     * @param int $visibility
     * @param OpenWeatherWindDTO $wind
     * @param OpenWeatherCloudsDTO $clouds
     * @param int $dt
     * @param OpenWeatherSysDTO $sys
     * @param int $timezone
     * @param string $cityName
     * @param int $responseCode
     */
    public function __construct(
        OpenWeatherCoordinationDTO $coordination,
        OpenWeatherDescribeDTO $weather,
        string $base,
        OpenWeatherGeneralDTO $main,
        int $visibility,
        OpenWeatherWindDTO $wind,
        OpenWeatherCloudsDTO $clouds,
        int $dt,
        OpenWeatherSysDTO $sys,
        int $timezone,
        string $cityName,
        int $responseCode,
    )
    {
        $this->coordination = $coordination;
        $this->weather = $weather;
        $this->base = $base;
        $this->main = $main;
        $this->visibility = $visibility;
        $this->wind = $wind;
        $this->clouds = $clouds;
        $this->dt = $dt;
        $this->sys = $sys;
        $this->timezone = $timezone;
        $this->cityName = $cityName;
        $this->responseCode = $responseCode;
    }

    public static function fromDTO(OpenWeatherMainDTO $dto): WeatherOutputDTO
    {
        return new WeatherOutputDTO(
            coordination: $dto->getCoordination(),
            weather: $dto->getWeather(),
            base: $dto->getBase(),
            main: $dto->getMain(),
            visibility: $dto->getVisibility(),
            wind: $dto->getWind(),
            clouds: $dto->getClouds(),
            dt: $dto->getDt(),
            sys: $dto->getSys(),
            timezone: $dto->getTimezone(),
            cityName: $dto->getCityName(),
            responseCode: $dto->getResponseCode(),
        );
    }

    /**
     * @param array $data
     * @return WeatherOutputDTO
     */
    public static function fromArray(array $data): WeatherOutputDTO
    {
        return new WeatherOutputDTO(
            coordination: $data['coordination'],
            weather: $data['weather'],
            base: $data['base'],
            main: $data['main'],
            visibility: $data['visibility'],
            wind: $data['wind'],
            clouds: $data['clouds'],
            dt: $data['dt'],
            sys: $data['sys'],
            timezone: $data['timezone'],
            cityName: $data['cityName'],
            responseCode: $data['responseCode'],
        );
    }

    public function toArray(): array
    {
        return [
            'coordination' => $this->getCoordination()->toArray(),
            'weather' => $this->getWeather()->toArray(),
            'base' => $this->getBase(),
            'main' => $this->getMain()->toArray(),
            'visibility' => $this->getVisibility(),
            'wind' => $this->getWind()->toArray(),
            'clouds' => $this->getClouds()->toArray(),
            'dt' => $this->getDt(),
            'sys' => $this->getSys()->toArray(),
            'timezone' => $this->getTimezone(),
            'cityName' => $this->getCityName(),
            'responseCode' => $this->getResponseCode(),
        ];
    }

    public function toResponse($request): JsonResponse|Response
    {
        return response()->json(
            [
                'data' => $this->toArray(),
            ],
            $this->getResponseCode()
        );
    }

    /**
     * @return OpenWeatherCoordinationDTO
     */
    public function getCoordination(): OpenWeatherCoordinationDTO
    {
        return $this->coordination;
    }

    /**
     * @return OpenWeatherDescribeDTO
     */
    public function getWeather(): OpenWeatherDescribeDTO
    {
        return $this->weather;
    }

    /**
     * @return string
     */
    public function getBase(): string
    {
        return $this->base;
    }

    /**
     * @return OpenWeatherGeneralDTO
     */
    public function getMain(): OpenWeatherGeneralDTO
    {
        return $this->main;
    }

    /**
     * @return int
     */
    public function getVisibility(): int
    {
        return $this->visibility;
    }

    /**
     * @return OpenWeatherWindDTO
     */
    public function getWind(): OpenWeatherWindDTO
    {
        return $this->wind;
    }

    /**
     * @return OpenWeatherCloudsDTO
     */
    public function getClouds(): OpenWeatherCloudsDTO
    {
        return $this->clouds;
    }

    /**
     * @return int
     */
    public function getDt(): int
    {
        return $this->dt;
    }

    /**
     * @return OpenWeatherSysDTO
     */
    public function getSys(): OpenWeatherSysDTO
    {
        return $this->sys;
    }

    /**
     * @return int
     */
    public function getTimezone(): int
    {
        return $this->timezone;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @return int
     */
    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->responseCode !== 200 ;
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->getMain()->getTemperature();
    }

    /**
     * @return float
     */
    public function getTempFeelsLike(): float
    {
        return $this->getMain()->getFeelsLike();
    }

    /**
     * @return float
     */
    public function getTempMin(): float
    {
        return $this->getMain()->getTempMin();
    }

    /**
     * @return float
     */
    public function getTempMax(): float
    {
        return $this->getMain()->getTempMax();
    }

    /**
     * @return string
     */
    public function getWindDirection(): string
    {
        if (empty($this->getWind()->getSpeed())) {
            return "No direction";
        }
        $cardinalDirections = [
            0   => 'Северный',
            23  => 'Северо-восточный',
            68  => 'Восточный',
            113 => 'Юго-восточный',
            158 => 'Южный',
            203 => 'Юго-западный',
            248 => 'Западный',
            293 => 'Северо-западный',
            338 => 'Северный',
        ];
        $resultDegree = 0;
        foreach ($cardinalDirections as $degree => $direction) {
            if ($this->getWind()->getDeg() < $degree) {
                break;
            }
            $resultDegree = $degree;
        }
        return $cardinalDirections[$resultDegree];
    }

    /**
     * @return float
     */
    public function getWindSpeed(): float
    {
        return $this->getWind()->getSpeed();
    }
}
