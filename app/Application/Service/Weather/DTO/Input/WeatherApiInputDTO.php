<?php
/**
 * Description of WeatherApiInputDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\DTO\Input;

class WeatherApiInputDTO
{
    private string $cityName;

    /**
     * @param string $cityName
     */
    public function __construct(string $cityName)
    {
        $this->cityName = $cityName;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            cityName: $data['city'],
        );
    }

    public function toArray(): array
    {
        return [
            'cityName' => $this->getCityName(),
        ];
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }
}
