<?php
/**
 * Description of OpenWeatherMap.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Weather\OpenWeatherMap;

use App\Application\Contract\Weather\WeatherInterface;
use App\Application\Service\Telegram\Helper\ErrorCodes;
use App\Application\Service\Weather\DTO\OpenWeatherMap\OpenWeatherMainDTO;
use App\Application\Service\Weather\DTO\Output\WeatherOutputDTO;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class OpenWeatherMap implements WeatherInterface
{

    const URL = 'api.openweathermap.org/data/2.5/weather';
    const UNITS = 'metric';

    /**
     * @var string
     */
    private string $token;

    /**
     * @var string|mixed
     */
    private string $apiUrl;

    /**
     * @var string
     */
    private string $units;

    /**
     * @var Response
     */
    private Response $response;


    public function __construct()
    {
        $this->token = $this->getToken();
        $this->apiUrl = $this->getUrl();
        $this->units = $this->getUnits();
    }

    /**
     * @param string $city
     * @return WeatherOutputDTO
     */
    public function getWeather(string $city): WeatherOutputDTO
    {
        try {
            $this->response = Http::get($this->apiUrl, [
                'q' => $city,
                'appid' => $this->token,
                'units' => $this->units,
                'lang' => App::getLocale(),
            ]);
        } catch (Throwable $e) {
            $error = $this->getError();
            Log::alert(ErrorCodes::getErrorMessage($error['cod']));
            return $this->getWeatherOutputDTO($error);
        }
        return $this->getWeatherOutputDTO($this->response->json());
    }

    /**
     * Возвращает токен
     *
     * @return string
     */
    private function getToken(): string
    {
        return $_ENV['WEATHER_TOKEN'];
    }

    /**
     * возвращает url ресурса OpenWeatherMap
     *
     * @return string
     */
    private function getUrl(): string
    {
        return $_ENV['WEATHER_URL'] ?? self::URL;
    }

    /**
     * Возвращает значение опции Units of measurement.
     * Возможные значения: standard, metric and imperial.
     * standard units - по умолчанию
     *
     * @return string
     */
    private function getUnits(): string
    {
        return $_ENV['WEATHER_UNITS'] ?? self::UNITS;
    }

    /**
     * @return array[]
     */
    private function getError(): array
    {
        return ['cod' => 500];
    }

    public function getWeatherOutputDTO(array $data): WeatherOutputDTO
    {
        return WeatherOutputDTO::fromDTO(OpenWeatherMainDTO::fromArray($data));
    }
}
