<?php
/**
 * Description of FilesRepository.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Repository;

class FilesRepository implements RepositoryInterface
{
    const CHAT_BUTTONS_MASK = 'chatbtns';
    const BUTTONS_MASK = 'buttons';

    private FileManager $fileManager;

    /**
     * @param FileManager $fileManager
     */
    public function __construct(FileManager $fileManager)
    {
        $this->fileManager = $fileManager;
    }


    /**
     * Возвращает набор кнопок для чата
     *
     * @param string $path
     * @return array
     */
    public function getChatButtons(string $path): array
    {
        return $this->fileManager->getDataFromFiles($path, self::CHAT_BUTTONS_MASK);
    }

    /**
     * Возвращает набор кнопок для панели
     *
     * @param $path
     * @return array
     */
    public function getButtons($path): array
    {
        return $this->fileManager->getDataFromFiles($path, self::BUTTONS_MASK);
    }
}
