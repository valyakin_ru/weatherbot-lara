<?php
/**
 * Description of RepositoryInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Repository;

interface RepositoryInterface
{
    public function getChatButtons(string $path): array;
    public function getButtons(string $path): array;
}
