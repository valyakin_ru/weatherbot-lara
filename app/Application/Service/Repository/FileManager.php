<?php
/**
 * Description of FileManager.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Repository;

class FileManager
{
    /**
     * Находит все файлы в директории $path, имена которых начинаются как маска $maskFileName
     * и возвращает их как массив с содержимым в виде массива кнопок
     *
     * @param string $path - путь, где производится поиск нужных файлов
     * @param string $maskFileName - маска, которая должна быть в названии файла
     * @return array
     */
    public function getDataFromFiles(string $path, string $maskFileName): array
    {
        $chatButtons = [];
        $dh = opendir($path);
        while ($file = readdir($dh)) {
            if ($file === '..' || $file === '.') {
                continue;
            }
            if (str_starts_with($file, $maskFileName)) {
                $chatButtons[$file] = json_decode(file_get_contents($path . '/' . $file), true);
            }
        }
        closedir($dh);
        return $chatButtons;
    }
}
