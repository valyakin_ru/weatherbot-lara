<?php
/**
 * Description of VerificationDTOInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\DTO\Output;

interface VerificationDTOInterface
{
    public function isActive(): bool;
}
