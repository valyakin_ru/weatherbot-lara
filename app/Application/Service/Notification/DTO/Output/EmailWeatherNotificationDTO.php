<?php
/**
 * Description of EmailWeatherRequestDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\DTO\Output;


use App\Application\Service\DTO\AbstractBaseDTO;
use App\Application\Service\Weather\DTO\Output\WeatherOutputDTO;
use App\Mail\WeatherShipped;
use Illuminate\Mail\Mailable;


class EmailWeatherNotificationDTO extends AbstractBaseDto implements NotificationOutputInterface
{
    private string $email;
    private WeatherOutputDTO $message;
    private string $subject = 'Бот погоды';

    /**
     * @param string $email
     * @param weatherOutputDTO $message
     */
    public function __construct(string $email, weatherOutputDTO $message)
    {
        $this->email = $email;
        $this->message = $message;
    }

    public static function fromArray(array $data): self
    {
        return new self(
            email: $data['email'],
            message: $data['message'],
        );
    }

    public function toArray(): array
    {
        return [
            'email' => $this->getEmail(),
            'message' => $this->getMessage(),
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return Mailable
     */
    public function getMessage(): Mailable
    {
        return new WeatherShipped($this->message);
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }
}
