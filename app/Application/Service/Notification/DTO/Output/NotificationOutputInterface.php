<?php
/**
 * Description of NotificationOutputInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\DTO\Output;

use Illuminate\Mail\Mailable;

interface NotificationOutputInterface
{
    public function getMessage(): Mailable;
}
