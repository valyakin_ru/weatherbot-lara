<?php
/**
 * Description of EmailWeatherVerificationDTO.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\DTO\Output;

use App\Mail\VerifyShipped;
use Illuminate\Mail\Mailable;

class EmailWeatherVerificationDTO implements VerificationDTOInterface, NotificationOutputInterface
{
    private string $email;
    private string $city;
    private string $sendAt;
    private bool $active;
    private string $verifyToken;

    /**
     * @param string $email
     * @param string $city
     * @param string $sendAt
     * @param bool $active
     * @param string $verifyToken
     */
    public function __construct(
        string $email,
        string $city,
        string $sendAt,
        bool $active,
        string $verifyToken,
    )
    {
        $this->email = $email;
        $this->city = $city;
        $this->sendAt = $sendAt;
        $this->active = $active;
        $this->verifyToken = $verifyToken;
    }


    /**
     * @param array $data
     * @return static
     */
    public static function fromArray(array $data): self
    {
        return new self(
            email: $data['email'],
            city: $data['city'],
            sendAt: $data['sendAt'],
            active: $data['active'],
            verifyToken: $data['verifyToken'],
        );
    }

    public function toArray(): array
    {
        return [
            'email' => $this->getEmail(),
            'city' => $this->getCity(),
            'sendAt' => $this->getSendAt(),
            'active' => $this->isActive(),
            'verifyToken' => $this->getVerifyToken(),
        ];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getSendAt(): string
    {
        return $this->sendAt;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getVerifyToken(): string
    {
        return $this->verifyToken;
    }

    /**
     * @return Mailable
     */
    public function getMessage(): Mailable
    {
        return new VerifyShipped($this);
    }
}
