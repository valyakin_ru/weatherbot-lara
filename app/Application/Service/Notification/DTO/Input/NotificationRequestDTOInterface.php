<?php
/**
 * Description of NotificationRequestDTOInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\DTO\Input;

interface NotificationRequestDTOInterface
{
    public function toArray(): array;
}
