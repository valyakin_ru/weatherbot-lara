<?php
/**
 * Description of NotificationInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification;


use App\Application\Service\Notification\DTO\Output\NotificationOutputInterface;
use App\Application\Service\Notification\DTO\Output\VerificationDTOInterface;
use Illuminate\Database\Eloquent\Model;


interface NotificationInterface
{
    public function notify(NotificationOutputInterface $dto): void;

    public function getVerificationDTO(array $parameters): VerificationDTOInterface;

    public function updateNotificationModel(VerificationDTOInterface $dto): Model;
}
