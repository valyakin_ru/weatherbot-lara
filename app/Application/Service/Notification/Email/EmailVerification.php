<?php
/**
 * Description of EmailVerification.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\Email;

use App\Application\Exceptions\Email\EmailDomainNotFoundException;
use App\Application\Exceptions\Email\InvalidEmailFormatException;
use App\Application\Exceptions\Telegram\TelegramTimeParameterErrorException;
use App\Application\Service\Notification\DTO\Output\EmailWeatherVerificationDTO;
use App\Application\Service\Validation\Email\EmailValidator;
use App\Domain\Models\WeatherEmailNotification;
use Exception;

class EmailVerification
{
    const INVALID_TIME_FORMAT = 'Не правильное время';
    const ACCESS_ACTIVATION_NOTIFICATION = 'Уведомление погоды на почту активировано';
    const TOKEN_ERROR = 'Ошибка подтверждения. Не верный Token';


    private EmailValidator $emailValidator;
    private array $parameters;

    /**
     * @param EmailValidator $emailValidator
     */
    public function __construct(EmailValidator $emailValidator)
    {
        $this->emailValidator = $emailValidator;
    }

    /**
     * @throws TelegramTimeParameterErrorException
     * @throws InvalidEmailFormatException
     * @throws EmailDomainNotFoundException
     * @throws Exception
     */
    public function getEmailNotificationDTO(array $parameters): EmailWeatherVerificationDTO
    {
        $this->parameters = $this->validateParameters($parameters);
        return EmailWeatherVerificationDTO::fromArray([
            'email' => $this->getEmail(),
            'city' => $this->getCity(),
            'sendAt' => $this->getSendAt(),
            'active' => $this->isEmailActive(),
            'verifyToken' => $this->getToken(),
        ]);
    }

    private function getEmail(): string
    {
        return $this->parameters[2];
    }

    private function isEmailActive(): bool
    {
        return ($this->parameters[4] !== 'off');

    }

    private function getSendAt(): string
    {
        return ($this->parameters[4] !== 'off')
            ? $this->parameters[4]
            : '00:00';
    }

    private function getCity(): string
    {
        return $this->parameters[3];
    }

    private function getToken(): string
    {
        return md5($this->getEmail().$this->getCity().$this->getSendAt().((string)rand(0, 10000000)));
    }

    /**
     * @param EmailWeatherVerificationDTO $emailWeatherVerificationDTO
     * @return WeatherEmailNotification
     */
    public function updateNotificationModel(EmailWeatherVerificationDTO $emailWeatherVerificationDTO): WeatherEmailNotification
    {
        return WeatherEmailNotification::updateOrCreate(
            ['email' => $emailWeatherVerificationDTO->getEmail(), 'city'=> $emailWeatherVerificationDTO->getCity()],
            [
                'active' => false,
                'send_at' => $emailWeatherVerificationDTO->getSendAt(),
                'verify_token' =>  $emailWeatherVerificationDTO->getVerifyToken(),
            ]
        );
    }

    /**
     * Устанавливает для текущей записи признак активности отправки оповещений на почту.
     *
     * @param WeatherEmailNotification $weatherEmailNotification
     */
    private function setActiveNotification(WeatherEmailNotification $weatherEmailNotification): void
    {
        $weatherEmailNotification->update(
            ['active' => true, 'verify_token' => md5((string)rand(0, 10000000))],
        );
    }
    /**
     * @param WeatherEmailNotification|null $weatherEmailNotification
     * @return string
     */
    public function verifyEmail(?WeatherEmailNotification $weatherEmailNotification): string
    {
        if ($weatherEmailNotification) {
            $this->setActiveNotification($weatherEmailNotification);
            return self::ACCESS_ACTIVATION_NOTIFICATION;
        }
        return self::TOKEN_ERROR;
    }

    /**
     * @throws EmailDomainNotFoundException
     * @throws InvalidEmailFormatException
     * @throws TelegramTimeParameterErrorException
     */
    private function validateParameters($parameters): array
    {
        $parameters[1] = $parameters[1] ?? '';
        $parameters[2] = $parameters[2] ?? '';
        $emailValid = $this->emailValidator->isValidEmail($parameters[2]);
        $parameters[3] = $parameters[3] ?? '';
        $parameters[4] = $parameters[4] ?? '';
        if ($parameters[4] !== 'off') {
            $this->validateTimeFormat($parameters[4]);
        }
        return $parameters;
    }

    /**
     * @throws TelegramTimeParameterErrorException
     */
    private function validateTimeFormat(string $time): void
    {
        if (!preg_match('/^(([0-9]|[0,1][0-9])|(2[0-3])):[0-5][0-9]$/', $time)) {
            throw new TelegramTimeParameterErrorException(self::INVALID_TIME_FORMAT);
        }
    }
}
