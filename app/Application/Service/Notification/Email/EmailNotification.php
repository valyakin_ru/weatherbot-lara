<?php
/**
 * Description of EmailNotification.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification\Email;


use App\Application\Service\Notification\DTO\Output\NotificationOutputInterface;
use Illuminate\Support\Facades\Mail;


class EmailNotification
{
    /**
     * Отправляет готовое сообщение по почте
     *
     * @param NotificationOutputInterface $emailNotificationDTO
     */
    public function sendEmail(NotificationOutputInterface $emailNotificationDTO): void
    {
        Mail::to($emailNotificationDTO->getEmail())->send($emailNotificationDTO->getMessage());
    }
}
