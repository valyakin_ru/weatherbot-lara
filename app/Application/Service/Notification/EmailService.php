<?php
/**
 * Description of EmailService.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Service\Notification;

use App\Application\Exceptions\Email\EmailDomainNotFoundException;
use App\Application\Exceptions\Email\InvalidEmailFormatException;
use App\Application\Exceptions\Telegram\TelegramTimeParameterErrorException;
use App\Application\Service\Notification\DTO\Output\EmailWeatherVerificationDTO;
use App\Application\Service\Notification\DTO\Output\NotificationOutputInterface;
use App\Application\Service\Notification\DTO\Output\VerificationDTOInterface;
use App\Application\Service\Notification\Email\EmailNotification;
use App\Application\Service\Notification\Email\EmailVerification;
use App\Domain\Models\WeatherEmailNotification;
use Illuminate\Database\Eloquent\Model;


class EmailService implements NotificationInterface
{

    private EmailNotification $emailNotification;
    private EmailVerification $emailVerification;


    public function __construct()
    {
        $this->emailNotification = $this->getEmailNotification();
        $this->emailVerification = $this->getEmailVerification();
    }

    /**
     * @param NotificationOutputInterface $dto
     */
    public function notify(NotificationOutputInterface $dto): void
    {
        $this->sendEmail($dto);
    }

    /**
     * @param array $parameters
     * @return EmailWeatherVerificationDTO
     * @throws EmailDomainNotFoundException
     * @throws InvalidEmailFormatException
     * @throws TelegramTimeParameterErrorException
     */
    public function getVerificationDTO(array $parameters): EmailWeatherVerificationDTO
    {
        return $this->emailVerification->getEmailNotificationDTO($parameters);
    }

    /**
     * @param VerificationDTOInterface $dto
     * @return Model
     */
    public function updateNotificationModel(VerificationDTOInterface $dto): Model
    {
        return $this->emailVerification->updateNotificationModel($dto);
    }

    /**
     * @param NotificationOutputInterface $dto
     */
    private function sendEmail(NotificationOutputInterface $dto): void
    {
        $this->emailNotification->sendEmail($dto);
    }

    /**
     * @param WeatherEmailNotification|null $weatherEmailNotification
     * @return string
     */
    public function verifyEmail(?WeatherEmailNotification $weatherEmailNotification): string
    {
        return $this->emailVerification->verifyEmail($weatherEmailNotification);
    }

    /**
     * @return EmailNotification
     */
    private function getEmailNotification(): EmailNotification
    {
        return app(EmailNotification::class);
    }

    /**
     * @return EmailVerification
     */
    private function getEmailVerification(): EmailVerification
    {
        return app(EmailVerification::class);
    }
}
