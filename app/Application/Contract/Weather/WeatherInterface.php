<?php
/**
 * Description of WeatherInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Contract\Weather;

use App\Application\Service\Weather\DTO\Output\WeatherOutputDTO;

/**
 * Интерфейс WeatherInterface содержит контракт для поставщиков услуг информации по погоде
 */
interface WeatherInterface
{
    /**
     * Получить данные о погоде
     *
     * @param string $city
     * @return WeatherOutputDTO
     */
    public function getWeather(string $city): WeatherOutputDTO;

    /**
     * Возвращает требуемую структуру данных
     *
     * @param array $data
     * @return WeatherOutputDTO
     */
    public function getWeatherOutputDTO(array $data): WeatherOutputDTO;
}
