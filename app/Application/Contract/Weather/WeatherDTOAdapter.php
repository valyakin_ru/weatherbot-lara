<?php
/**
 * Description of WeatherDTOAdapter.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Application\Contract\Weather;

use App\Application\Service\Weather\DTO\WeatherOutputDto;

interface WeatherDTOAdapter
{
    public function getWeatherOutputDTO(array $data): WeatherOutputDto;

}
