<?php
/**
 * Description of TelegramCommandInterface.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Domain\Entities\Telegram;


interface TelegramCommandInterface
{
    /**
     * Запускает выполнение команды пришедшей, из телеграмма
     */
    public function execute(): void;
}
