<?php

namespace App\Domain\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class WeatherBotChat extends Model implements InterfaceBotChat
{
    use HasFactory;

    protected $fillable = [
        'chat_id',
        'messenger_type',
        'active',
        'notification',
        'created_at',
        'updated_at',
    ];


    /**
     * @return int
     */
    public function getChatId(): int
    {
        return $this->chat_id;
    }

    /**
     * @param int $chat_id
     * @return WeatherBotChat
     */
    public function setChatId(int $chat_id): WeatherBotChat
    {
        $this->chat_id = $chat_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getMessengerType(): int
    {
        return $this->messenger_type;
    }

    /**
     * @param int $messenger_type
     * @return WeatherBotChat
     */
    public function setMessengerType(int $messenger_type): WeatherBotChat
    {
        $this->messenger_type = $messenger_type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return WeatherBotChat
     */
    public function setActive(bool $active): WeatherBotChat
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNotification(): bool
    {
        return $this->notification;
    }

    /**
     * @param bool $notification
     * @return WeatherBotChat
     */
    public function setNotification(bool $notification): WeatherBotChat
    {
        $this->notification = $notification;
        return $this;
    }

    /**
     * @param bool $active
     * @return bool
     */
    public function saveActiveStatus(bool $active): bool
    {
        return $this->setActive($active)->save();
    }

    /**
     * @param bool $notification
     * @return bool
     */
    public function saveNotificationStatus(bool $notification): bool
    {
        return $this->setNotification($notification)->save();
    }
}
