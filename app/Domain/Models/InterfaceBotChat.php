<?php
/**
 * Description of InterfaceBotChat.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Domain\Models;

interface InterfaceBotChat
{
    public function isActive(): bool;
    public function isNotification(): bool;
    public function saveActiveStatus(bool $active): bool;
    public function saveNotificationStatus(bool $notification): bool;
}
