<?php

namespace App\Domain\Models;;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherEmailNotification extends Model
{
    use HasFactory;

    protected $fillable = [
        'send_at',
        'email',
        'city',
        'created_at',
        'active',
        'verify_token',
        'updated_at',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];
}
