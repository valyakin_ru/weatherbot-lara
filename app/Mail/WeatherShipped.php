<?php

namespace App\Mail;

use App\Application\Service\Weather\DTO\Output\WeatherOutputDTO;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WeatherShipped extends Mailable
{
    use Queueable, SerializesModels;

    public WeatherOutputDTO $weatherDTO;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(WeatherOutputDTO $data)
    {
        $this->weatherDTO = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('weather.notification');
    }
}
