<?php

namespace App\Mail;

use App\Application\Service\Notification\DTO\Output\EmailWeatherVerificationDTO;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class VerifyShipped extends Mailable
{
    use Queueable, SerializesModels;

    const URI_VERIFICATION = '/notification/verify?token=';

    public EmailWeatherVerificationDTO $noticeDTO;
    public string $verificationUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(EmailWeatherVerificationDTO $data)
    {
        $this->noticeDTO = $data;
        $this->verificationUrl = URL::to($this->getUrl());
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('weather.verify');
    }

    /**
     * @return string
     */
    private function getUrl(): string
    {
        return self::URI_VERIFICATION . $this->noticeDTO->getVerifyToken();
    }
}
