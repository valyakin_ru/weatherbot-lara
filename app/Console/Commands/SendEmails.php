<?php

namespace App\Console\Commands;

use App\Application\Service\Notification\DTO\Input\EmailWeatherRequestDTO;
use App\Application\Service\Notification\DTO\Output\EmailWeatherNotificationDTO;
use App\Application\Service\Notification\EmailService;
use App\Application\Service\Weather\WeatherService;
use App\Domain\Models\WeatherEmailNotification;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Date;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a weather mail to Telegram user';

    /**
     * Сервис погоды
     *
     * @var WeatherService
     */
    private WeatherService $weatherService;

    /**
     * Почтовый сервис
     *
     * @var EmailService
     */
    private EmailService $notificationService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->weatherService = $this->getWeatherService();;
        $this->notificationService = $this->getEmailService();

        $items = $this->getWeatherEmailNotifications();
        foreach ($items as $item) {
            $dto = $this->getDTO($item);
            $this->notify($dto);
        }
    }

    private function notify(EmailWeatherRequestDTO $dto): void
    {
        $this->notificationService->notify(EmailWeatherNotificationDTO::fromArray([
            'email' => $dto->getEmail(),
            'message' => $this->weatherService->getWeather($dto->getCity())
        ]));
    }

    private function getWeatherEmailNotifications(): collection
    {
        $now = Date::now()->format('H:i');
        return  WeatherEmailNotification::where(['active' => true])
            ->where(['send_at' => $now])->get();
    }

    private function getDTO(WeatherEmailNotification $item): EmailWeatherRequestDTO
    {
        return EmailWeatherRequestDTO::fromArray($item->toArray());
    }

    /**
     * @return EmailService
     */
    private function getEmailService(): EmailService
    {
        return app(EmailService::class);
    }

    /**
     * @return WeatherService
     */
    private function getWeatherService(): WeatherService
    {
        return app(WeatherService::class);
    }
}
