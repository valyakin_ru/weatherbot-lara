<?php

namespace App\Providers;

use App\Application\Contract\Weather\WeatherInterface;
use App\Application\Service\Weather\OpenWeatherMap\OpenWeatherMap;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public array $bindings = [
        WeatherInterface::class => OpenWeatherMap::class,
    ];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
