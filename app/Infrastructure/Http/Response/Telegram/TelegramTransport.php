<?php
/**
 * Description of TelegramTransport.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Response\Telegram;


use App\Application\Exceptions\Telegram\TelegramSendErrorException;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendButtonDTO;
use App\Application\Service\Telegram\DTO\Output\Transport\TelegramSendMessageDTO;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;


class TelegramTransport
{
    const TELEGRAM_ERROR = 'Telegram Api error';

    private string $url;
    private string $method;
    private string $token;
    private array $message;


    public function __construct()
    {
        $this->url = env('TELEGRAM_URL');
        $this->token = env('TELEGRAM_TOKEN');
    }

    /**
     * Отправляет текстовое сообщение
     *
     * @param TelegramSendMessageDTO $message
     */
    public function sendMessage(TelegramSendMessageDTO $message): void
    {
        $this->setMessage($message->toArray())
            ->setMethod('SendMessage')
            ->send();
    }

    public function sendButtons(TelegramSendButtonDTO $buttons): void
    {
        $this->setMessage($buttons->toArray())
            ->setMethod('SendMessage')
            ->send();
    }

    public function sendVideo(): void
    {

    }

    public function sendPhoto(): void
    {

    }

    /**
     * Отправляет данные в телеграмм
     */
    private function send(): void
    {
        try {
            $response = Http::post($this->getUrlString(), $this->message);
            if (!$response->successful()) {
                throw new TelegramSendErrorException($response->getReasonPhrase(), $response->status());
            }
        } catch (TelegramSendErrorException $e) {
            Log::alert(self::TELEGRAM_ERROR . " " . $response->status() . ": " . $response->getReasonPhrase());
        } catch (ConnectionException $e) {
            Log::alert($e->getMessage());
        }
    }

    /**
     * @param string $url
     * @return TelegramTransport
     */
    public function setUrl(string $url): TelegramTransport
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string $method
     * @return TelegramTransport
     */
    public function setMethod(string $method): TelegramTransport
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param array $message
     * @return TelegramTransport
     */
    public function setMessage(array $message): TelegramTransport
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param string $token
     * @return TelegramTransport
     */
    public function setToken(string $token): TelegramTransport
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    private function getUrlString(): string
    {
        return $this->url . $this->token . "/". $this->method;
    }
}
