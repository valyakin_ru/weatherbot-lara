<?php
/**
 * Description of TelegramWebHookController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Telegram;


use App\Infrastructure\Http\Controllers\Telegram\Request\TelegramWebHookRequest;


class TelegramWebHookController extends BaseTelegramController
{

    public function __invoke(TelegramWebHookRequest $request)
    {
        $this->inputDTO = $request->getDTO();
        $this->weatherBotChat = $this->getWeatherBotChat();

        $this->getTelegramService()->runBot();
    }
}
