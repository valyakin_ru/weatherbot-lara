<?php
/**
 * Description of TelegramWebHookRequest.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Telegram\Request;



use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use Illuminate\Foundation\Http\FormRequest;

class TelegramWebHookRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'update_id' => [
                'required',
            ],
            'callback_query' => [

            ],
            'message' => [

            ],
            'edited_message' => [

            ],
            'data' => [

            ],
        ];
    }

    public function getDTO(): TelegramInputDTO
    {
        return TelegramInputDTO::fromArray($this->validated());
    }
}
