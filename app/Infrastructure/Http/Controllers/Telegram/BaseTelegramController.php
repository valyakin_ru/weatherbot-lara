<?php
/**
 * Description of BaseTelegramController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Telegram;

use App\Application\Service\Telegram\DTO\Input\TelegramInputDTO;
use App\Application\Service\Telegram\TelegramService;
use App\Domain\Models\WeatherBotChat;
use App\Infrastructure\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;


class BaseTelegramController extends Controller
{

    protected TelegramInputDTO $inputDTO;
    protected TelegramService $telegramService;
    protected WeatherBotChat $weatherBotChat;


    protected function bind(): void
    {
        App::bind(TelegramInputDTO::class, function ($app) {
            return $this->inputDTO;
        });
        App::bind(WeatherBotChat::class, function ($app) {
            return $this->weatherBotChat;
        });
    }

    /**
     * @return TelegramService
     */
    protected function getTelegramService(): TelegramService
    {
        $this->bind();
        return app(TelegramService::class);
    }

    protected function getWeatherBotChat(): WeatherBotChat
    {
        return WeatherBotChat::firstOrCreate(
            ['messenger_type' => TelegramService::TELEGRAM_MESSENGER_TYPE, 'chat_id' => $this->inputDTO->getChatId()],
            ['active' => false, 'notification' => env('TELEGRAM_NOTIFICATION')]
        );
    }
}
