<?php
/**
 * Description of WeatherRequest.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api\V1\Weather\Request;

use App\Application\Service\Weather\DTO\Input\WeatherApiInputDTO;
use Illuminate\Foundation\Http\FormRequest;

class WeatherRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'city' => [
                'required',
            ],
        ];
    }

    public function getDTO(): WeatherApiInputDTO
    {
        return WeatherApiInputDTO::fromArray($this->validated());
    }
}
