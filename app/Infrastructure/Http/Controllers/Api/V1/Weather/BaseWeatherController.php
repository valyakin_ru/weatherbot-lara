<?php
/**
 * Description of BaseWeatherController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api\V1\Weather;

use App\Application\Service\Weather\WeatherService;
use App\Infrastructure\Http\Controllers\Controller;

class BaseWeatherController extends Controller
{
    protected WeatherService $weatherService;

    /**
     * @param WeatherService $weatherService
     */
    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

}
