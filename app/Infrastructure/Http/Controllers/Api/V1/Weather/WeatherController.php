<?php
/**
 * Description of WeatherController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Api\V1\Weather;

use App\Infrastructure\Http\Controllers\Api\V1\Weather\Request\WeatherRequest;

class WeatherController extends BaseWeatherController
{
    public function __invoke(WeatherRequest $request)
    {
        $dto = $request->getDTO();
        return $this->weatherService->getWeather($dto->getCityName())->toResponse($request);
    }
}
