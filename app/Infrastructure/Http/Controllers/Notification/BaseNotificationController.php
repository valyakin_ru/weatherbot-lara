<?php
/**
 * Description of BaseNotificationController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Notification;


use App\Application\Service\Notification\DTO\Input\EmailWeatherRequestDTO;
use App\Domain\Models\WeatherEmailNotification;
use App\Infrastructure\Http\Controllers\Controller;


class BaseNotificationController extends Controller
{
    protected WeatherEmailNotification $WeatherEmailNotification;
    protected EmailWeatherRequestDTO $request;
}
