<?php
/**
 * Description of EmailVerifyController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Notification;

use App\Application\Service\Notification\EmailService;
use App\Infrastructure\Http\Controllers\Notification\Request\EmailVerificationRequest;

class EmailVerifyController extends BaseNotificationController
{
    private EmailService $notificationService;


    /**
     * @param EmailService $notificationService
     */
    public function __construct(EmailService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function __invoke(EmailVerificationRequest $request)
    {
        $verifyResult = $this->notificationService->verifyEmail($request->getWeatherEmailNotification());
        return response($verifyResult);
    }
}
