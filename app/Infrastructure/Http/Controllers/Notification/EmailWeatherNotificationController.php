<?php
/**
 * Description of EmailWeatherNotificationController.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Notification;


use App\Application\Service\Notification\DTO\Output\EmailWeatherNotificationDTO;
use App\Application\Service\Notification\EmailService;
use App\Application\Service\Weather\WeatherService;
use App\Infrastructure\Http\Controllers\Notification\Request\EmailWeatherNotificationRequest;


class EmailWeatherNotificationController extends BaseNotificationController
{
    private WeatherService $weatherService;
    private EmailService $notificationService;


    /**
     * @param WeatherService $weatherService
     * @param EmailService $notificationService
     */
    public function __construct(WeatherService $weatherService, EmailService $notificationService)
    {
        $this->weatherService = $weatherService;
        $this->notificationService = $notificationService;
    }

    public function __invoke(EmailWeatherNotificationRequest $request)
    {
        $dto = $request->getDTO();
        $this->notificationService->notify(EmailWeatherNotificationDTO::fromArray([
            'email' => $dto->getEmail(),
            'message' => $this->weatherService->getWeather($dto->getCity())
        ]));
    }
}
