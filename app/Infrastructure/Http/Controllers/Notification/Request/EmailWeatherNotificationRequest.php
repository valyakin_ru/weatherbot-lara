<?php
/**
 * Description of EmailWeatherNotificationRequest.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Notification\Request;

use App\Application\Service\Notification\DTO\Input\EmailWeatherRequestDTO;
use Illuminate\Foundation\Http\FormRequest;

class EmailWeatherNotificationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => [
                'email',
                'required',
            ],
            'city' => [
                'required'
            ],
            'sendAt' => [
                'required'
            ],
            'active' =>[

            ],
        ];
    }

    public function getDTO(): EmailWeatherRequestDTO
    {
        return EmailWeatherRequestDTO::fromArray($this->validated());
    }
}
