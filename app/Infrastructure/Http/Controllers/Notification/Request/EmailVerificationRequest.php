<?php
/**
 * Description of EmailVerificationRequest.php
 * @copyright Copyright (c) valyakin.ru
 * @author    Vladimir Valyakin <vladimir@valyakin.ru>
 */

declare(strict_types=1);

namespace App\Infrastructure\Http\Controllers\Notification\Request;

use App\Domain\Models\WeatherEmailNotification;
use Illuminate\Foundation\Http\FormRequest;

class EmailVerificationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'token' => [
                'required',
            ],
        ];
    }

    /**
     * @return WeatherEmailNotification|null
     */
    public function getWeatherEmailNotification(): ?WeatherEmailNotification
    {
        return WeatherEmailNotification::where('verify_token', $this->validated())->first();
    }
}
